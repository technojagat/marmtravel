<?php

/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
class Post_activity extends MY_Model
{
	protected $_table_name = 'post_activities';
	protected $_primary_key = 'post_activities.id';
	protected $_order_by = 'id';
	protected $_timestamps = TRUE;
	public $rules = array(
		'day_eng' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'trim|required|max_length[100]|xss_clean'
		)
		/*,
		'slug' => array(
			'field' => 'slug',
			'label' => 'Slug',
			'rules' => 'trim|required|max_length[100]|url_title|callback__unique_slug|xss_clean'
		),
		'category_id[]' => array(
			'field' => 'category_id[]',
			'label' => 'Category',
			'rules' => 'trim|required|is_natural|xss_clean'
		),
		'body' => array(
			'field' => 'body',
			'label' => 'Body',
			'rules' => 'trim|required'
		),
		'pubdate' => array(
			'field' => 'pubdate',
			'label' => 'Publication date',
			'rules' => 'trim|required|exact_length[10]|xss_clean'
		),
		'image' => array(
			'field' => 'image',
			'label' => 'Image',
			'rules' => 'trim|xss_clean'
		)*/
	);



	public function get_new ()
	{
		$article = new stdClass();
		$article->day_eng = '';
		$article->activity_eng = '';
		$article->hotel_eng = '';
		$article->details_eng = '';
		$article->day_turk = '';
		$article->activity_turk = '';
		$article->hotel_turk = '';
		$article->details_turk = '';
		//$article->user_id = $this->session->userdata('account_id');
       /* $map = directory_map(APPPATH . 'language', 1);
        foreach($map as $lang) {
        	$lang = str_replace(DIRECTORY_SEPARATOR, '', $lang);
            if(is_dir(APPPATH . 'language/'.$lang) and $lang != 'english') {
              $title = 'title_'.$lang;
              $body = 'body_'.$lang;
              $article->$title = '';
              $article->$body = '';
            }
        }*/

		return $article;
	}

	public function get($id = NULL, $single = FALSE, $published = FALSE, $category = NULL)
	{
		//$this->db->where('post_activities.post_type', 'post');
		$this->db->select('post_activities.*');
		
        if($published != FALSE){
		    $this->set_published();
        }
        
        $this->_primary_key = 'post_activities.id';
		return parent::get($id, $single);
	}

	public function get_by_post_id($post_id, $id = NULL, $single = FALSE)
	{
		$this->db->where('post_activities.post_id', $post_id);
        //$this->db->where('post_activities.category_id', $cat_id);
		return $this->get($id, $single);
	}

    public function get_last($limit = null, $offset = 0, $category = null) {
		$this->db->where('post_activities.post_type', 'post');
		if(!is_null($limit)) {
        	$this->db->limit($limit, $offset);
		}
    	$this->set_published();
		return $this->get(NULL, FALSE, TRUE, $category);
    }

    public function get_results($limit = null, $offset = 0, $like) {

		$this->db->or_like($like);

		if(!is_null($limit)) {
        	$this->db->limit($limit, $offset);
		}

    	$this->set_published();

		return $this->get(NULL, FALSE, TRUE, NULL);
    }

    public function get_next($id) {
		$this->db->where('post_activities.post_type', 'post');
        $this->db->where('post_activities.id >', $id);
        $this->set_published();
        $this->db->limit(1);
        $this->_order_by = "post_activities.id ASC";
		return $this->get(NULL, TRUE);
    }

    public function get_previuse($id) {
		$this->db->where('post_activities.post_type', 'post');
        $this->db->where('post_activities.id <', $id);
        $this->set_published();
        $this->db->limit(1);
        $this->_order_by = "post_activities.id DESC";
		return $this->get(NULL, TRUE);
    }

    public function get_slider_post_activities($limit = null, $post_activities = array(), $cats = array(), $expect_post_activities = array(), $expect_cats = array())
    {
    	if(!empty($limit)) {
    		$this->db->limit( $limit );
    	}
    	if(!empty($post_activities)) {
    		$this->db->where_in( 'post_activities.id', $post_activities );
    	}
    	if(!empty($cats)) {
    		$this->db->where_in( 'post_activities.category_id', $cats );
    	}
    	if(!empty($expect_post_activities)) {
    		$this->db->where_not_in( 'post_activities.id', $expect_post_activities );
    	}
    	if(!empty($expect_cats)) {
    		$this->db->where_not_in( 'post_activities.category_id', $expect_cats );
    	}

    	$this->db->order_by('post_activities.id', 'DESC');

    	return $this->get(null, FALSE, TRUE);

    }

	public function get_by_id($id = NULL, $single = FALSE, $published = FALSE)
	{
        if($published != FALSE){
		    $this->set_published();
        }
        $this->_primary_key = 'post_activities.id';
		return $this->get($id, $single);
	}


	public function set_published(){
		$this->db->where('pubdate <=', date('Y-m-d'));
		$this->db->where('statue', 1);
	}

	public function get_recent($limit = 3){
		
		// Fetch a limited number of recent post_activities
		$limit = (int) $limit;
		$this->db->where('post_activities.post_type', 'post');
		$this->set_published();
		$this->db->limit($limit);
		return $this->get();
	}

}