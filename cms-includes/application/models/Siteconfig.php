<?php

/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
class Siteconfig extends CI_Model {

 public function __construct()
 {
    parent::__construct();
 }

 public function get_all()
 {
    return $this->db->get('config_data');
 }

}