<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/* Actions */
$lang['delete'] = 'Silmek  %s';
$lang['save'] = 'Kayıt Etmek %s';
$lang['savepublish'] = 'Kaydet ve yayınla';
$lang['all'] = 'Herşey %s';
$lang['add'] = 'Eklemek %s';
$lang['edit'] = 'Düzenle %s';
$lang['date'] = 'Tarih';
$lang['created'] = 'Üzerinde düzenlendi';
$lang['publish'] = 'Yayınla %s';
$lang['unpublish'] = 'Yayından %s';
$lang['deleted']='% S’iniz başarıyla silindi';
$lang['saved']='% S’iniz başarıyla kaydedildi';
$lang['updated']='% S’iniz başarıyla güncellendi';
$lang['published']='% S’iniz başarıyla yayınlandı';
$lang['unpublished']='% S’iniz başarıyla yayından kaldırıldı';
$lang['check_errors'] = 'Lütfen aşağıdaki hataları kontrol edin!';
$lang['access_denied'] = 'Bu sayfaya erişim izniniz yok';
$lang['general'] = 'Genel';
$lang['contact'] = 'Temas';
$lang['home']='Ev';
$lang['visit_site']='Siteyi Ziyaret Et';
$lang['dashboard']='Gösterge Paneli';
$lang['media']='Medya';
$lang['modules']='Modüller';
$lang['module']='Modül';

/* Posts */
$lang['posts']='Turlar';
$lang['post']='Tur';
$lang['post_image']='Tur Resmi';
$lang['cust_options']='Özel seçenekler';
$lang['latest_posts']='Son Turlar';
$lang['publication_date']='Yayın tarihi';
$lang['post_translate']='Tur çeviri';
$lang['sidebar']='Kenar çubuğu';
$lang['topBar']='Üst Bar';
$lang['post_comments']='Tur yorumları';
$lang['all_posts']='Tüm Turlar';
$lang['add_post']='Tur ekle';

/* Pages */
$lang['pages']='Sayfalar';
$lang['page']='Sayfa';
$lang['page_order']='Sayfa sırası';
$lang['page_image']='Sayfa resmi';
$lang['page_translate']='Sayfa çevirisi';
$lang['in_menu']='Menüde';
$lang['page_comments']='Sayfa yorumları';
$lang['all_pages']= 'Tüm sayfalar';
$lang['add_page']='Sayfa ekle';

/* Categories */
$lang['categories']='Kategoriler';
$lang['latest_categories']='Son Kategoriler';
$lang['category']='Kategori';
$lang['all_categories']='Tüm Kategoriler';
$lang['add_category']='Kategori ekle';

/* Languages */
$lang['languages']='duujjil';
$lang['language']='Dil';
$lang['select_language']='Dil Seçin';
$lang['translate_language']='Dili çevir';

/* Slider */
$lang['sliders']='kaydırıcılar';
$lang['slider']='kaydırıcı';
$lang['slides']='Slaytlar';
$lang['slide']='Kaymak';
$lang['all_sliders']='Tüm sürgü';
$lang['add_slider']='Kaydırıcı ekle';

/* Control */
$lang['action']='Aksiyon';
$lang['id']='İD';
$lang['title']='Başlık';
$lang['alias']='takma ad';
$lang['shortcode']='Kısa kod';
$lang['body']='içerik';
$lang['content']='Içerik';
$lang['statue']='heykel';
$lang['image']='görüntü';
$lang['current_image']='Geçerli resim';
$lang['select_image']='Fotoğraf seç';
$lang['change_image']='Resmi değiştir';
$lang['type']='tip';
$lang['slug']='sümüklüböcek';
$lang['order']='Sipariş';
$lang['link']='bağlantı';
$lang['parent']='ebeveyn';
$lang['comment']='Soruşturma';
$lang['comments']='Araştırma';
$lang['leave_comment']='Bir Soruşturma Bırakın';
$lang['submit_comment']='Sorgulama Gönder';
$lang['your_comment']='Soruşturma';
$lang['has_comments']='% S sorguları var';
$lang['delete_confirm_msg']='Bunu% s silmek istediğinizden emin misiniz?';
$lang['general_settings']='Genel Ayarlar';
$lang['contact_settings']='İletişim Ayarları';

/* Users */
$lang['users']='Kullanıcılar';
$lang['user']='kullanıcı';
$lang['user_groups']='Kullanıcı Grupları';
$lang['permissions']='İzinler';
$lang['permission']='izin';
$lang['role']='rol';
$lang['roles']='Roller';
$lang['login_dashboard_msg']='Kontrol paneline erişmek için lütfen giriş yapın';
$lang['login']='Oturum aç';
$lang['signin']='oturum aç';
$lang['logout']='Çıkış Yap';
$lang['username']='Kullanıcı adı';
$lang['password']='Parola';
$lang['account_settings']='Hesap ayarları';
$lang['email']='E-posta';
$lang['group']='grup';
$lang['register_date']='Kayıt Tarihi';
$lang['banned']='yasaklı';
$lang['ban_user']='Bu kullanıcıyı yasakla';
$lang['unban_user']='Bu kullanıcıyı yasaklama';
$lang['active']='Aktif';
$lang['login_remember']='Beni Hatırla';
$lang['forgot_password']='Şifremi Unuttum';
$lang['all_users']='Tüm kullanıcılar';
$lang['add_user']='Kullanıcı Ekle';
$lang['all_roles']='Tüm rolleri';
$lang['add_role']='Rol ekle';
$lang['all_permissions']='Tüm izinler';
$lang['add_permission']='İzin ekle';


$lang['contact']='Temas';
$lang['services']='Hizmetler';
$lang['last_works']='Son Çalışmalar';
$lang['about']='hakkında';
$lang['settings']='Ayarlar';
$lang['settings']='% s Ayarlar';
$lang['content']='içerik';
$lang['system']='System';

$lang['profile']='Profil';
$lang['themes']='Temalar';
$lang['theme']='Tema';
$lang['active_theme']='Temayı etkinleştir';
$lang['current_theme']='Mevcut tema';
$lang['theme_options']='Tema ayarları';
$lang['theme_info']='Tema bilgisi';
$lang['apperance']='Görünüm';
$lang['menu']='Menü';
$lang['menus']='Menüler';
$lang['all_menus']='Tüm menüler';
$lang['add_menu']='Menü ekle';
$lang['widgets']='widget\'lar';
$lang['widget']='Widget';

/* Menu Options */
$lang['explore'] = 'keşfetmek';
$lang['menu_options'] = 'Menü seçenekleri';
$lang['others_options'] = 'Diğer Seçenekler';

/* account general */
$lang['sign_in_sign_in'] = 'Oturum aç';

/* Tour Page Options */
$lang['itinerary'] = 'Yolculuk Özeti';
$lang['date'] = 'tarih';
$lang['activity'] = 'Aktivite';
$lang['hotel'] = 'Otel';
$lang['price_heading'] = 'Fiyat bilgisi';
$lang['flight_heading'] = 'Önerilen Uçuş Detayları';
$lang['tour_subscribe_heading'] = 'Bu turla ilgileniyor musunuz? <br /> Bizimle iletişime geç.';
$lang['tour_download'] = 'Yol programını indir';
$lang['send_your_friend'] = 'Gönder arkadaş';
$lang['send_to_friend'] = 'Arkadaşa gönder';
/* Send to friend form */
$lang['your_name'] = 'Adınız';
$lang['your_email'] = 'E-posta adresiniz';
$lang['recipient_name'] = 'Alıcının ismi';
$lang['recipient_email'] = 'Alıcının e-postası';
$lang['subject'] = 'konu';
$lang['message'] = 'mesaj';
$lang['send_button'] = 'göndermek';

/* let's help you form */
$lang['helpform_head'] = 'Bir soru sor';
$lang['namesurname'] = 'Ad Soyad';
$lang['phonenum'] = 'Telefon numarası';

/* Popup Options */
$lang['popup_email'] = 'E-posta kimliğinizi girin <br> daha fazla ayrıntı için';
$lang['success_email_msg'] = '<strong>başarı!</strong> Aboneliğiniz başarıyla gönderildi.';

/* Footer Options */
$lang['subscribe_title'] = 'Bizim abone ol<span>Bülten</span>';
$lang['subscribe_placeholder'] = 'Email adresinizi giriniz';
$lang['subscribe_button'] = 'Abone ol';
$lang['links_title'] = 'Bağlantılar';
$lang['gallery_title'] = 'galeri';
$lang['ok'] = 'Tamam';
$lang['contact_to_marm_travel'] = 'Sorularınız için lütfen bizimle iletişime geçin contact@marmtravel.com';

/* Form submit Options */
$lang['success_form_msg'] = 'Bilgi başarıyla gönderildi.';
