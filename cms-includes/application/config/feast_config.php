<?php

$config['site_name'] = 'FEAST CMS';
$config['demo_site'] = FALSE;
$config['templates_path'] = 'cms-content/themes/';
$config['timezone'] = 'Africa/Cairo';