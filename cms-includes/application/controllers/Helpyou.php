<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
class Helpyou extends Frontend_Controller {

    /**
     * Set Default frontpage.
     *
     * @return mixed load view file
     */
	public function index()
	{

        // Set page title for title tag
        $data['page_title'] = feast_line('Help you');

        // Set view file
        $data['main_content'] = 'helpyou';

        // Load view file with data
        $this->load->view($this->pref->active_theme.'/layouts/main',$data);
        
	}    
}
