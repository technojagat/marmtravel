<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
class Searchresults extends Frontend_Controller {

    /**
     * Set Default frontpage.
     *
     * @return mixed load view file
     */
	public function index()
	{

        // Set page title for title tag
		$search=$this->input->get('search', TRUE);
		if(!empty($search)){
			$search_term =$search;
		}else{
		 $search_term = $this->uri->segment(3);
		}
		 $condition="slug=\"".$search_term."\" OR title like '%".$search_term."%' ";
		$list=$this->Category_m->get_where($condition);
		$catids=[];
		$condition="1 AND  ";
		
		$i=0;
		if(count($list)>0){
			$condition .="( title like '%".$search_term."%' OR ";  
			foreach($list as $lists){
				if($i==0){
				$condition .="FIND_IN_SET('".$lists->id."',category_id)";
				}else{
				$condition .=" OR FIND_IN_SET('".$lists->id."',category_id)";
				}
				$i++;
			}
			$condition .=")";
		}else{
			$condition .="  title like '%".$search_term."%' ";  
		}
		$condition .=" AND `statue`=1 ";  
		//$this->db->select('id');
		$this->db->where($condition);
		$query=$this->db->get('posts');
		$data['searchtotal']=$query->num_rows();
		$data['searchpost']=$query->result();
		
        $data['page_title'] = feast_line('Search Results');

        // Set view file
        $data['main_content'] = 'search-results';

        // Load view file with data
        $this->load->view($this->pref->active_theme.'/layouts/main',$data);
        
	}    
}
