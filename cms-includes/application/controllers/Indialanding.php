<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
class Indialanding extends Frontend_Controller {

    /**
     * Set Default frontpage.
     *
     * @return mixed load view file
     */
	public function index()
	{

        // Set page title for title tag
        $data['page_title'] = feast_line('India landing');

        // Set view file
        $data['main_content'] = 'india-landing';

        // Load view file with data
        $this->load->view($this->pref->active_theme.'/layouts/main',$data);
        
	}    
}
