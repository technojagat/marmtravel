<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
class Home extends Frontend_Controller {

    /**
     * Set Default frontpage.
     *
     * @return mixed load view file
     */
	public function index()
	{ 


        // Load menu items
		$data['menu'] = $this->Page_m->get();

        // Load sliders
		$data['sliders'] = $this->Slide_m->get();

        // Set page title for title tag
        $data['page_title'] = feast_line('home');

        // Set view file
        $data['main_content'] = 'comingsoon';

        // Load view file with data
        $this->load->view($this->pref->active_theme.'/comingsoon',$data);
        
	}


	public function main()
	{ 
/*	$lng=$this->session->userdata('site_lang');
	if(empty($lng)){
	$this->session->set_userdata('site_lang', 'turkish');
	}*/

        // Load menu items
		$data['menu'] = $this->Page_m->get();

        // Load sliders
		$data['sliders'] = $this->Slide_m->get();

        // Set page title for title tag
        $data['page_title'] = feast_line('home');

        // Set view file
        $data['main_content'] = 'index';

        // Load view file with data
        $this->load->view($this->pref->active_theme.'/layouts/main',$data);
        
	}
    /**
     * Contact function.
     *
     * @return string return sent message statue
     */
    public function contact()
    {

        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Message from website</h1>
            
            	<ul>
            		<li><span>Name : </span> ".$_POST['name']."</li>
            		<li><span>Email : </span> ".$_POST['email']."</li>
            		<li><span>Message : </span> ".$_POST['message']."</li>
            	</ul>
        	</div>
        ";

        // Set email options
        $this->email->from($_POST['email'], $_POST['name']);
        $this->email->to( $this->pref->contact_email );
        $this->email->subject('Message from website');
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
	
//=================
    public function sendsubscribe()
    {

        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Subscribe from website</h1>
            
            	<ul>
            		<li><span>Email : </span> ".$_POST['data']."</li>
            		
            	</ul>
        	</div>
        ";

        // Set email options
        $this->email->from('admin@marmtravel.com','Marm Travel');
        $this->email->to($this->pref->contact_email);
        $this->email->subject('Subscribe from website');
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
  
    public function sendafriend()
    {

$data = array();
foreach(explode('&', urldecode($_POST['data'])) as $value)
{
    $value1 = explode('=', $value);
    $data[$value1[0]] = ($value1[1]);
}
        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Message from Marm Travel</h1>
            
            	<ul>
            		<li><span>Name : </span> ".$data['name']."</li>
					<li><span>Recipient's Name : </span> ".$data['recipient']."</li>
            		<li><span>Email : </span> ".$data['email']."</li>
            		<li><span>Message : </span> ".$data['message']."</li>
					<li><span>Url : </span><a href='".$data['refurl']."'>Click here</a></li>
					
            	</ul>
        	</div>
        ";

        // Set email options
       $this->email->from('admin@marmtravel.com','Marm Travel');
        $this->email->to($data['to']);
	    //$this->email->to('csebidhan@gmail.com');
        $this->email->subject($data['subject']);
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }  
//=================
    public function sendsubscribetour()
    {
$data = array();
foreach(explode('&', urldecode($_POST['data'])) as $value)
{
    $value1 = explode('=', $value);
    $data[$value1[0]] = ($value1[1]);
}
        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Interested in this tour</h1>
            
            	<ul>
            		<li><span>Email : </span> ".$_POST['email']."</li>
            		<li><span>Url : </span><a href='".$_POST['refurl']."'>".$_POST['refurl']."</a></li>
            	</ul>
        	</div>
        ";

        // Set email options
        $this->email->from('admin@marmtravel.com','Marm Travel');
        $this->email->to($this->pref->contact_email);
        $this->email->subject('Interested Tour');
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
//=================
    public function marmcorporate()
    {

$data = array();
foreach(explode('&', urldecode($_POST['data'])) as $value)
{
    $value1 = explode('=', $value);
    $data[$value1[0]] = ($value1[1]);
}
        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Message from Marm Travel</h1>
            
            	<ul>
            		<li><span>Company Name : </span> ".$data['company-name']."</li>
					<li><span>Contact Name : </span> ".$data['contact-name']."</li>
            		<li><span>Contact Person Phone No. : </span> ".$data['contact-phone']."</li>
					<li><span>Contact Person Email : </span> ".$data['contact-email']."</li>					
            		<li><span>Address : </span> ".$data['address']."</li>
					<li><span>Query : </span> ".$data['query']."</li>
					
					
            	</ul>
        	</div>
        ";

        // Set email options
       $this->email->from('admin@marmtravel.com','Marm Travel');
        $this->email->to($this->pref->contact_email);
	    //$this->email->to('csebidhan@gmail.com');
        $this->email->subject($data['subject']);
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
//=================
    public function hotel()
    {

$data = array();
foreach(explode('&', urldecode($_POST['data'])) as $value)
{
    $value1 = explode('=', $value);
    $data[$value1[0]] = ($value1[1]);
}
        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Hotel Booking from Marm Travel</h1>
            
            	<ul>
				<li><span>Name: </span> ".$data['name']."</li>
					<li><span>Email: </span> ".$data['email']."</li>
					<li><span>Phone: </span> ".$data['phone']."</li>
            		<li><span>Destination/Hotel/Landmarks : </span> ".$data['query']."</li>
					<li><span>Check In : </span> ".$data['check-in']."</li>
            		<li><span>Check Out : </span> ".$data['check-out']."</li>
            		<li><span>Adult (12 age) : </span> ".$data['adult']."</li>
					<li><span>Chindren (2-12 age) : </span> ".$data['chindren']."</li>
					<li><span>No of Room : </span> ".$data['Room']."</li>
					
					
            	</ul>
        	</div>
        ";

        // Set email options
       $this->email->from('admin@marmtravel.com','Marm Travel');
        $this->email->to($this->pref->contact_email);
	    //$this->email->to('csebidhan@gmail.com');
        $this->email->subject('Hotel Booking');
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
//=================
    public function flight()
    {

$data = array();
foreach(explode('&', urldecode($_POST['data'])) as $value)
{
    $value1 = explode('=', $value);
    $data[$value1[0]] = ($value1[1]);
}
        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Flight Booking from Marm Travel</h1>
            
            	<ul>
            		<li><span>Name: </span> ".$data['name']."</li>
					<li><span>Email: </span> ".$data['email']."</li>
					<li><span>Phone: </span> ".$data['phone']."</li>
					<li><span>From: </span> ".$data['from']."</li>
					<li><span>To : </span> ".$data['to']."</li>            		
            		<li><span>Adult (12 age) : </span> ".$data['adult']."</li>
					<li><span>Chindren (2-12 age) : </span> ".$data['chindren']."</li>
					<li><span>Infant (0-2 age) : </span> ".$data['infant']."</li>
					
					
            	</ul>
        	</div>
        ";

        // Set email options
       $this->email->from('admin@marmtravel.com','Marm Travel');
        $this->email->to($this->pref->contact_email);
	    //$this->email->to('csebidhan@gmail.com');
        $this->email->subject('Flight Booking');
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
//=================
    public function helpyou()
    {

$data = array();
foreach(explode('&', urldecode($_POST['data'])) as $value)
{
    $value1 = explode('=', $value);
    $data[$value1[0]] = ($value1[1]);
}
        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Message from Marm Travel</h1>
            
            	<ul>
            		<li><span>Name : </span> ".$data['company-name']."</li>
					<li><span>Email : </span> ".$data['email']."</li>
            		<li><span>Phone No. : </span> ".$data['designation']."</li>					
					<li><span>Query : </span> ".$data['query']."</li>
					
					
            	</ul>
        	</div>
        ";

        // Set email options
       $this->email->from('admin@marmtravel.com','Marm Travel');
        $this->email->to($this->pref->contact_email);
	    //$this->email->to('csebidhan@gmail.com');
        $this->email->subject($data['subject']);
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
//=============================
    public function testemail()
    {

        // Load required libraries
        $this->load->library('email');

        // Set mailtype to html
        $this->email->set_mailtype("html");

        // Set message content
        $message = "
            <style type='text/css'>
            	* {
            		padding: 0;
            		margin: 0;
            		outline: none;
            		text-decoration: none;
            	}
            	#message {
                	font-family: 'Open Sans', sans-serif;
                	line-height: 1.5;
                	font-size: 14px;
                	padding: 15px;
            	}
            	h1 {
            		font-size: 22px;
            		color: #19BA61;
            		padding-bottom: 10px;
            		border-bottom: 1px solid #19BA61;
            		margin-bottom: 20px;
            		font-weight: 700;
            	}
            
            	ul {
            		list-style: none;
            		margin: 0;
            	}
            
            	li span {
            		color: #19BA61;
            		font-weight: 400
            	}
            
            	li {
            		color: #2c3e50;
            		margin-bottom: 10px;
            		font-weight: 300
            	}
            </style>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'>
             <div id='message'>
            	<h1>Message from website</h1>
            
            	<ul>
            		<li><span>Name : </span>".$this->pref->contact_email."</li>
            		
            	</ul>
        	</div>
        ";

        // Set email options
        $this->email->from('admin@marmtravel.com', 'Marm Travel');
        $this->email->to( 'csebidhan@gmail.com' );
        $this->email->subject('Message from website');
        $this->email->message( $message );

        // Check if message sent or have errors        
        if( $this->email->send() ) {
            echo 'Your message has been sent successfully';
        } else {
            echo $_POST['email'].'\n'.$_POST['name'].'\n'.$_POST['message'].'\n'.$this->pref->contact_email;
        }     

    }
}
