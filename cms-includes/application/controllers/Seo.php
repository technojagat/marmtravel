<?php
 
/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
Class Seo extends CI_Controller {

    function sitemap()
    {

        $data['posts']      = $this->Post_m->get();
        $data['pages']      = $this->Page_m->get();
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->load->ext_view('admin', 'sitemap',$data);
    }
}