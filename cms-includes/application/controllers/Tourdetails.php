<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Bidhan Baral
 * Email: bidhan.baral@gmail.com
 * Year:2018
 *
 */
class Tourdetails extends Frontend_Controller {

    /**
     * Set Default frontpage.
     *
     * @return mixed load view file
     */
	public function index()
	{

        // Set page title for title tag
        $data['page_title'] = feast_line('Tour Details');
		$search_term = $this->uri->segment(3);
		 $condition="slug=\"".$search_term."\"";
		$this->db->where($condition);
		$query=$this->db->get('posts');
		$data['searchtotal']=$query->num_rows();
		$result=$query->result();
		
		$data['searchpost']=$result;
		$this->db->where('post_id',$result[0]->id);
		$query=$this->db->get('post_activities');
		$data['totalactivity']=$query->num_rows();
		$resultactivity=$query->result();
		$data['postactivity']=$resultactivity;
		
		$this->db->where('post_id',$result[0]->id);
		$query=$this->db->get('post_meta');
		$data['totalmeta']=$query->num_rows();
		$postmeta=$query->result();
		$data['postmeta']=$postmeta;
        // Set view file
        $data['main_content'] = 'marm-tour';
		 $data['page_title']=$result[0]->title;

        // Load view file with data
        $this->load->view($this->pref->active_theme.'/layouts/main',$data);
        
	}    
}
