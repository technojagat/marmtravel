<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

  <div class="row" ng-app="postAct" ng-controller="postActController">
  
  
<?php echo form_open();?>
			<div class="col-md-9"><?php echo form_submit('submit', feast_line('save'), 'class="btn btn-primary btn-lg"');?></div>
     		  <div ng-repeat="activity in postActivity" ng-init="row=$index">
              <div class="col-md-9">
                  <div class="form-group" style="margin-right: 0; margin-left: 0">
                      <label for="exampleInputEmail1">day {{$index+1}} </label>
                        <input type="text" class="form-control" ng-model="activity.day_eng" name="day_eng[]"/>
                  </div>
              </div>
              <div class="col-md-9">
                  <div class="form-group" style="margin-right: 0; margin-left: 0">
                      <label for="exampleInputEmail1">Activity {{$index+1}} </label>
                        <input type="text" class="form-control" ng-model="activity.activity_eng" name="activity_eng[]"/>
                        <input ng-hide="true" type="text" class="form-control" ng-model="activity.id" name="postid[]"/>
						<input ng-hide="true" type="text" class="form-control" ng-model="activity.is_deleted" name="is_deleted[{{$index}}]"/>
						
                  </div>
              </div>
              <div class="col-md-9">
                  <div class="form-group" style="margin-right: 0; margin-left: 0">
                      <label for="exampleInputEmail1">hotel {{$index+1}} </label>
                        <input type="text" class="form-control" ng-model="activity.hotel_eng" name="hotel_eng[]"/>
                  </div>
              </div>
                <div class="col-md-9">
              		<div class="form-group" style="margin-right: 0; margin-left: 0">
                  <label>Details {{$index+1}}</label>
                  <?php echo form_textarea('','', array('class' => 'form-control tinymce','ui-tinymce'=>'tinymceOptions','name'=>'details_eng[]',"ng-model"=>"activity.details_eng")); ?>
                  <?php //echo form_error($body, '<p class="text-danger">', '</p>'); ?>
              </div>
              </div>
              <div class="col-md-9">turkish</div>
              <div class="col-md-9">
                  <div class="form-group" style="margin-right: 0; margin-left: 0">
                      <label for="exampleInputEmail1">day {{$index+1}} </label>
                        <input type="text" class="form-control" ng-model="activity.day_turkish" name="day_turkish[]"/>
                  </div>
              </div>
              <div class="col-md-9">
                  <div class="form-group" style="margin-right: 0; margin-left: 0">
                      <label for="exampleInputEmail1">Activity {{$index+1}} </label>
                        <input type="text" class="form-control" ng-model="activity.activity_turkish" name="activity_turkish[]"/>
                  </div>
              </div>
              <div class="col-md-9">
                  <div class="form-group" style="margin-right: 0; margin-left: 0">
                      <label for="exampleInputEmail1">hotel {{$index+1}} </label>
                        <input type="text" class="form-control" ng-model="activity.hotel_turkish" name="hotel_turkish[]"/>
                  </div>
              </div>
                <div class="col-md-9">
                        <div class="form-group" style="margin-right: 0; margin-left: 0">
                        <label>Details {{$index+1}}</label>
                        <?php echo form_textarea('','', array('class' => 'form-control tinymce',  'ui-tinymce'=>'tinymceOptions', 'name'=>'details_turkish[]',"ng-model"=>"activity.details_turkish")); ?>
                        
                        </div>
              </div> 
			  <div class="col-md-9">
                        <div class="form-group" style="margin-right: 0; margin-left: 0">
                        <label>Delete {{$index+1}}</label>
                      <input type="checkbox" ng-model="activity.is_deleted"
          value="1" name="is_deleted[{{$index}}]">
                      
                        </div>
              </div>
               <div  class="col-md-4" ng-init="upd($index)">End Day {{$index+1}}  
             <div class="col-md-1" ng-show="postActivity.length==$index+1"> <button type="button" ng-click="add($index)" class="btn btn-sm btn-primary">Add Next Day</button></div>
             
          </div>
          
          </div>
        <?php echo form_close();?>
    
    
  </div>