<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

  <div class="row">
    <?php echo form_open();?>
    <div class="col-md-12">

      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#english" data-toggle="tab">English</a></li>
          <?php
            $map = directory_map(APPPATH . 'language', 1);
            $d = 0;
            foreach($map as $lang) {
            $lang = str_replace(DIRECTORY_SEPARATOR, '', $lang);
              if(is_dir(APPPATH . 'language/'.$lang) and $lang != 'english') { ?>
          <li><a data-toggle="tab" href="#<?=$lang?>"><?=$lang?></a></li>
          
          <?php
              }
              $d++;
            }
          ?>
        </ul>

        <div class="tab-content">
          <div id="english" class="tab-pane fade in active">
            <div class="form-group" style="margin-right: 0; margin-left: 0">
              <?php
              echo form_label(feast_line('title'),'title');
              echo form_input('title',set_value('title',$category->title),'class="form-control"');
              echo form_error('title', '<p class="text-danger">', '</p>');
              ?>
            </div>
            <div class="form-group" style="margin-right: 0; margin-left: 0">
                <label><?=feast_line('body')?></label>
                <?php echo form_textarea('body', set_value('body', $category->body, FALSE), array('class' => 'form-control tinymce', 'id' => 'body')); ?>
                <?php echo form_error('body', '<p class="text-danger">', '</p>'); ?>
            </div>
          </div>
      <?php
        $map = directory_map(APPPATH . 'language', 1);
        $c = 0;
        foreach($map as $lang) {
        $lang = str_replace(DIRECTORY_SEPARATOR, '', $lang);
          if(is_dir(APPPATH . 'language/'.$lang) and $lang != 'english') {
          $title = 'title_'.$lang;
          $body = 'body_'.$lang;
          ?>
          <div id="<?=$lang?>" class="tab-pane fade">
              <div class="form-group" style="margin-right: 0; margin-left: 0">
                  <label for="exampleInputEmail1"><?=feast_line('title')?></label>
                  <?php echo form_input($title, set_value($title, $category->$title), array('class' => 'form-control')); ?>
                        <?php echo form_error($title, '<p class="text-danger">', '</p>'); ?>
              </div>
              <div class="form-group" style="margin-right: 0; margin-left: 0">
                  <label><?=feast_line('body')?></label>
                  <?php echo form_textarea($body, set_value($body, $category->$body, FALSE), array('class' => 'form-control tinymce', 'id' => 'body')); ?>
                  <?php echo form_error($body, '<p class="text-danger">', '</p>'); ?>
              </div>
          </div>
      <?php
          $c++;
          }
        }
      ?>
        </div>
      </div>

        <div class="form-group">
          <?php
          echo form_label(feast_line('slug'),'slug');
          echo form_input('slug',set_value('slug',$category->slug),'class="form-control"');
          echo form_error('slug', '<p class="text-danger">', '</p>');
          ?>
        </div>

        <div class="form-group">

          <?php
          $list = array(0 => 'No parent');
          foreach (list_cats() as $option):
            if($option->id != @$this->uri->segment(4)) {
              $list[$option->id] = $option->title;
            }
          endforeach;

          echo form_label('Parent','parent_id');
          echo form_dropdown('parent_id', $list, $category->parent_id);
          echo form_error('parent_id', '<p class="text-danger">', '</p>');
          ?>
        </div>
       
             <?php /*?>     <?php if($this->uri->segment(4)){ ?>

                  <h4><?=feast_line('current_image')?></h4>
                  <img src="<?php echo $category->image; ?>" class="img-responsive" alt="<?php echo $category->title; ?>" />
                  <hr>
                  <?php } ?><?php */?>

                  <div class="form-group">
                      <div class="input-group input-group-sm">
                        <input class="form-control iframe-btn" placeholder="<?=$this->uri->segment(4) ? feast_line('change_image') : feast_line('select_image')?>" value="<?php echo $category->image; ?>" type="text" name="image" id="image">
                        <span class="input-group-btn">
                          <a href="#" class="btn btn-danger delete_media" id="image" data-toggle="tooltip" data-title="remove" type="button"><i class="fa fa-trash"></i></a>
                          <a href="<?=base_url()?>cms-includes/admin/assets/filemanager/dialog.php?type=1&field_id=image" class="btn btn-success iframe-btn" data-toggle="tooltip" data-title="<?=feast_line('select_image')?>" type="button"><i class="fa fa-folder-open"></i></a>
                        </span>
                      </div>
                      <div id="image_preview" class="thumbnail" style="margin-top: 10px; display: none">
                        <img src="" class="img-responsive">
                      </div>
                  </div>
               
        <div class="form-group">
          <?php
          echo form_label(feast_line('order'),'order');
          echo form_input('order',set_value('order',$category->order),'class="form-control"');
          echo form_error('order', '<p class="text-danger">', '</p>');
          ?>
        </div>
        <?php echo form_submit('submit', feast_line('save'), 'class="btn btn-primary btn-lg"');?>
    </div>
    <?php echo form_close();?>
  </div>