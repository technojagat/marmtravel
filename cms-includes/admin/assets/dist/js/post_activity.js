
var app = angular.module('postAct', ['ui.tinymce']);
app.controller('postActController', ['$scope', '$http','$timeout','$sce' ,function ($scope, $http,$timeout,$sce) {
	
		$scope.postActivity=data;
		
		$scope.tinymceOptions = {
     menubar:false,
                verify_html: false,
                editor_deselector: "simple",
                forced_root_block : "", 
                force_br_newlines : true,
                force_p_newlines : false,
                theme: "modern",
                skin: "bootmce",
                height: 300,
                relative_urls : false,
                remove_script_host: false,
        
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                     "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager mfnsc fontawesome noneditable background sourcebeautifier",
               ],
               toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager | print preview media fullpage | forecolor backcolor emoticons | fullscreen code sourcebeautifier background mfnsc icons progress",
                external_plugins: { "filemanager" : "http://marmtravel.com/cms-includes/admin/assets/filemanager/plugin.min.js", "mfnsc" : "http://marmtravel.com/cms-content/themes/themeone/tinymce/plugin.min.js"},
       
               style_formats: [
                  {title: "Headers", items: [
                      {title: "Header 1", format: "h1"},
                      {title: "Header 2", format: "h2"},
                      {title: "Header 3", format: "h3"},
                      {title: "Header 4", format: "h4"},
                      {title: "Header 5", format: "h5"},
                      {title: "Header 6", format: "h6"}
                  ]},
                  {title: "Inline", items: [
                      {title: "Bold", icon: "bold", format: "bold"},
                      {title: "Italic", icon: "italic", format: "italic"},
                      {title: "Underline", icon: "underline", format: "underline"},
                      {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
                      {title: "Superscript", icon: "superscript", format: "superscript"},
                      {title: "Subscript", icon: "subscript", format: "subscript"},
                      {title: "Code", icon: "code", format: "code"}
                  ]},
                  {title: "Blocks", items: [
                      {title: "Paragraph", format: "p"},
                      {title: "Blockquote", format: "blockquote"},
                      {title: "Div", format: "div"},
                      {title: "Pre", format: "pre"}
                  ]},
                  {title: "Alignment", items: [
                      {title: "Left", icon: "alignleft", format: "alignleft"},
                      {title: "Center", icon: "aligncenter", format: "aligncenter"},
                      {title: "Right", icon: "alignright", format: "alignright"},
                      {title: "Justify", icon: "alignjustify", format: "alignjustify"}
                  ]}
                ],
                visualblocks_default_state: false,
                valid_elements : "*[*]",
                valid_children : "+a[div]",
                body_class: "blue",
                extended_valid_elements: "i[*]|iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
                content_css :"http://marmtravel.com/cms-content/themes/themeone/assets/lib/bootstrap/dist/css/bootstrap.min.css,http://marmtravel.com/cms-content/themes/themeone/assets/lib/animate.css/animate.css,http://marmtravel.com/cms-content/themes/themeone/assets/lib/components-font-awesome/css/font-awesome.min.css,http://marmtravel.com/cms-content/themes/themeone/assets/lib/flexslider/flexslider.css,http://marmtravel.com/cms-content/themes/themeone/assets/css/colors/default.css,http://marmtravel.com/cms-content/themes/themeone/assets/css/style.css,http://marmtravel.com/cms-content/themes/themeone/assets/lib/et-line-font/et-line-font.css",
                external_filemanager_path:"http://marmtravel.com/cms-includes/admin/assets/filemanager/",
                filemanager_title:"Responsive Filemanager",
                setup: function(ed) {
                  ed.on("init", function() {
                   //tinymce.ScriptLoader.load("http://marmtravel.com/cms-content/themes/themeone/assets/js/main.js");
                  });
                }
  };
 //$timeout(function(){tinit();},100);

		if($scope.postActivity.length==0){
			$scope.postActivity=[{id: "0", post_id: postid, day_eng: "", activity_eng: "", hotel_eng: "",activity_turkish: "",day_turkish: "",details_turkish: "",hotel_turkish: "",is_deleted:"0"}];
			
			}else{
				for(var i=0;i<$scope.postActivity.length;i++){
					$scope.postActivity[i].activity_eng=$sce.trustAsHtml($scope.postActivity[i].activity_eng);
					
				}
				
				
			}
	console.log($scope.postActivity);
	
	$scope.add=function(index){
		
		
		$scope.postActivity.push({id: "0", post_id: postid, day_eng: "", activity_eng: "", hotel_eng: "",activity_turkish: "",day_turkish: "",details_turkish: "",hotel_turkish: "",is_deleted:"0"});
		
		
		
		};
		 function tinit(){
			alert('ss');
			 tinymce.init({
                mode: "textareas",
                menubar:false,
                verify_html: false,
                editor_deselector: "simple",
                forced_root_block : "", 
                force_br_newlines : true,
                force_p_newlines : false,
                theme: "modern",
                skin: "bootmce",
                height: 300,
                relative_urls : false,
                remove_script_host: false,
        
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                     "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager mfnsc fontawesome noneditable background sourcebeautifier",
               ],
               toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager | print preview media fullpage | forecolor backcolor emoticons | fullscreen code sourcebeautifier background mfnsc icons progress",
              
       
               style_formats: [
                  {title: "Headers", items: [
                      {title: "Header 1", format: "h1"},
                      {title: "Header 2", format: "h2"},
                      {title: "Header 3", format: "h3"},
                      {title: "Header 4", format: "h4"},
                      {title: "Header 5", format: "h5"},
                      {title: "Header 6", format: "h6"}
                  ]},
                  {title: "Inline", items: [
                      {title: "Bold", icon: "bold", format: "bold"},
                      {title: "Italic", icon: "italic", format: "italic"},
                      {title: "Underline", icon: "underline", format: "underline"},
                      {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
                      {title: "Superscript", icon: "superscript", format: "superscript"},
                      {title: "Subscript", icon: "subscript", format: "subscript"},
                      {title: "Code", icon: "code", format: "code"}
                  ]},
                  {title: "Blocks", items: [
                      {title: "Paragraph", format: "p"},
                      {title: "Blockquote", format: "blockquote"},
                      {title: "Div", format: "div"},
                      {title: "Pre", format: "pre"}
                  ]},
                  {title: "Alignment", items: [
                      {title: "Left", icon: "alignleft", format: "alignleft"},
                      {title: "Center", icon: "aligncenter", format: "aligncenter"},
                      {title: "Right", icon: "alignright", format: "alignright"},
                      {title: "Justify", icon: "alignjustify", format: "alignjustify"}
                  ]}
                ],
                visualblocks_default_state: false,
                valid_elements : "*[*]",
                valid_children : "+a[div]",
                body_class: "blue",
                extended_valid_elements: "i[*]|iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
                
                filemanager_title:"Responsive Filemanager"
               
             });

		 }
			 
		
}]);

	
