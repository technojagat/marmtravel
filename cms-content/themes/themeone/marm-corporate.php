

			<!-- Cover Image -->

			<div class="col-md-12 no-pad pad-left turkey-cover slider-eff">

				<img src="<?php echo get_option('marmcorporate_image_background'); ?>" alt="Marm Corporate">

				<div class="caption-inside"><?php echo  get_option_lng('marmcorporate_title'); ?></div>

			</div>

			<!-- Cover Image -->

			<div class="col-md-12 no-marg no-pad turkey-bg-n">

			<div class="col-md-3 side-panel right turkey-margin caption-nav nav-right-border">

				<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>

			</div>

			<div class="col-md-9 marm-corporate-desc">

				<br><br><p><?php echo  get_option_lng('marmcorporate_content'); ?></p>


<?php $sliders=get_slider("marm-corporate");?>
                <div class="col-md-12  no-marg no-pad top-marg top-marg">

                   <?php if($sliders){				



					 foreach( $sliders as $sl){ ?>



                <div class="col-md-4 wedding-img ">

                   

                        		<img class="img-thumbnail" src="<?php echo $sl['background']; ?>" alt="">

                  
					</div>



                <?php }} ?>

		  

			</div>
<div class="clearfix">&nbsp;</div>
				<?php /*?><div class="marm-corporte-details">

					<div class="marm-corporte-bg">

						<img src="<?php echo theme_folder('themeone'); ?>assets/img/wing-red.png" alt="">

					</div>

					<div class="col-md-10 tour-md-10 icon-listing">

						<h1><?php echo  get_option_lng('marmcorporate_heading'); ?></h1>

						<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/best-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmcorporate_services1'); ?> </li> 

                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/team-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services2'); ?></li>

                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/hotels-icn.png" width="25px" style=" vertical-align: sub;" lt=""> <?php echo  get_option_lng('marmcorporate_services3') ;?></li>

                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/travel-manag-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services4') ;?></li>

                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/meetings-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services5'); ?></li>

                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/visa-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services6') ;?></li>

					</div>

				</div><?php */?>

				<div class="requirement-form">

					<h1><?php echo  get_option_lng('formheading'); ?></h1>

					   <form id="marmcorporate_form" role="form" class="form-horizontal">

                       <input type="hidden" name="subject" value="Marm Corporate" >

					    <label class="control-label col-sm-4" for="company-name"><?php echo  get_option_lng('formfield_1'); ?>:</label>

					    <input type="text" class="form-control" id="company-name"  name="company-name" placeholder="<?php echo  get_option_lng('formfield_1');?>" required>

					    <label class="control-label col-sm-4" for="contact-name"><?php echo  get_option_lng('formfield_2');?>:</label>

					    <input type="text" class="form-control" id="contact-name" name="contact-name" placeholder="<?php echo  get_option_lng('formfield_2');?>" required>

						<label class="control-label col-sm-4" for="contact-phone"><?php echo  get_option_lng('formfield_6');?>:</label>

						<input type="text" class="form-control" id="contact-phone" name="contact-phone" placeholder="<?php echo  get_option_lng('formfield_6');?>" required>

						<label class="control-label col-sm-4" for="contact-email"><?php echo  get_option_lng('formfield_7');?>:</label>

						<input type="text" class="form-control" id="contact-email" name="contact-email" placeholder="<?php echo  get_option_lng('formfield_7');?>" required>

					  <?php /*?>  <label class="control-label col-sm-4" for="designation"><?php echo  get_option_lng('formfield_3');?>:</label>

					    <input type="text" class="form-control" id="designation" name="designation" placeholder="<?php echo  get_option_lng('formfield_3');?>" required><?php */?>

					    <label class="control-label col-sm-4" for="address"><?php echo  get_option_lng('formfield_4');?>:</label>

					    <input type="text" class="form-control" id="address" name="address" placeholder="<?php echo  get_option_lng('formfield_4');?>" required>

					    <label class="control-label col-sm-4" for="query"><?php echo  get_option_lng('formfield_5');?>:</label>

					    <textarea id="query" name="query" placeholder="<?php echo  get_option_lng('formfield_5');?>" required></textarea>

						

					    <div style="margin: 0 auto;width: 100%;text-align: center;">

                         <div class="alert alert-success" style="display:none;"  id="submit_marmcorporate">

						 <?= lang('success_form_msg');?>

  </div>

					    	<input type="submit" name="submit"  value="<?php echo  get_option_lng('submitbutton_title');?>">

                             <i id="submit_marmcorporatespin" class="fa fa-spinner fa-spin" style="display:none;font-size:24px;" ></i>

						</div>



					</form>

				</div>

			</div>
            </div>