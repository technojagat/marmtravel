// JavaScript Document

/*$(window).load(function () {
    var in_view = new Waypoint.Inview({
        element: $('#pocso-icon img, #jj-icon img, #rte-icon img, .home-service-box h2, .home-service-box p, #imp-links')[0],
        enter: function() {
            $('#pocso-icon img, #jj-icon img, #rte-icon img').addClass('animated flipInX');
			$('.home-service-box h2, .home-service-box p').addClass('animated bounceInUp');
			
        },
        exit: function() {  // optionally
            $('#pocso-icon img, #jj-icon img, #rte-icon img').removeClass('animated flipInX');
			$('.home-service-box h2, .home-service-box p').removeClass('animated bounceInUp');
			
			
        }
    });
});*/

$(window).load(function () {
    var in_view = new Waypoint.Inview({
        element: $('#pocso-icon img')[0],
        enter: function() {
            $('#pocso-icon img').addClass('animated flipInX');
			
        },
        exit: function() {  // optionally
            $('#pocso-icon img').removeClass('animated flipInX');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#jj-icon img')[0],
        enter: function() {
            $('#jj-icon img').addClass('animated flipInX');
			
        },
        exit: function() {  // optionally
            $('#jj-icon img').removeClass('animated flipInX');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#jj-icon img')[0],
        enter: function() {
            $('#rte-icon img').addClass('animated flipInX');
			
        },
        exit: function() {  // optionally
            $('#rte-icon img').removeClass('animated flipInX');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('.home-service-box h2')[0],
        enter: function() {
            $('.home-service-box h2').addClass('animated bounceInUp');
			
        },
        exit: function() {  // optionally
            $('.home-service-box h2').removeClass('animated bounceInUp');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('.home-service-box p')[0],
        enter: function() {
            $('.home-service-box p').addClass('animated bounceInUp');
			
        },
        exit: function() {  // optionally
            $('.home-service-box p').removeClass('animated bounceInUp');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#imp-links')[0],
        enter: function() {
            $('#imp-links').addClass('animated bounceInLeft');
			
        },
        exit: function() {  // optionally
            $('#imp-links').removeClass('animated bounceInLeft');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#news-events')[0],
        enter: function() {
            $('#news-events').addClass('animated bounceIn');
			
        },
        exit: function() {  // optionally
            $('#news-events').removeClass('animated bounceIn');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#child-line')[0],
        enter: function() {
            $('#child-line').addClass('animated bounceInRight');
			
        },
        exit: function() {  // optionally
            $('#child-line').removeClass('animated bounceInRight');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#magazine')[0],
        enter: function() {
            $('#magazine').addClass('animated bounceIn');
			
        },
        exit: function() {  // optionally
            $('#magazine').removeClass('animated bounceIn');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#swapper')[0],
        enter: function() {
            $('#swapper').addClass('animated bounceIn');
			
        },
        exit: function() {  // optionally
            $('#swapper').removeClass('animated bounceIn');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#commission-btn')[0],
        enter: function() {
            $('#commission-btn').addClass('animated bounceInUp');
			
        },
        exit: function() {  // optionally
            $('#commission-btn').removeClass('animated bounceInUp');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#child-right-btn')[0],
        enter: function() {
            $('#child-right-btn').addClass('animated bounceInUp');
			
        },
        exit: function() {  // optionally
            $('#child-right-btn').removeClass('animated bounceInUp');
			
        }
    });
	var in_view = new Waypoint.Inview({
        element: $('#hearing-btn')[0],
        enter: function() {
            $('#hearing-btn').addClass('animated bounceInUp');
			
        },
        exit: function() {  // optionally
            $('#hearing-btn').removeClass('animated bounceInUp');
			
        }
    });
});

/*$(document).ready(function() {
	$("#pocso-icon img").mouseover(function(e) {
    $(this).addClass('animated pulse');
	}).mouseout(function(e) {
		$(this).removeClass('animated pulse');
	});
	   
});	*/

/*$(document).ready(function() {
	$("#posco-icon img").hover(function(){
		$("this").addClass("animated bounce");								   
	})	
	
});*/

$(document).ready(function() {
	$("#fb-social img").mouseover(function(e) {
    $(this).attr('src','images/fb-social-icon.png');
	}).mouseout(function(e) {
		$(this).attr('src','images/fb-social-icon2.png');
	});
	
	$("#tw-social img").mouseover(function(e) {
    $(this).attr('src','images/tw-social-icon.png');
	}).mouseout(function(e) {
		$(this).attr('src','images/tw-social-icon2.png');
	});
	
	$("#yt-social img").mouseover(function(e) {
    $(this).attr('src','images/yt-social-icon.png');
	}).mouseout(function(e) {
		$(this).attr('src','images/yt-social-icon2.png');
	});
	
	$("#pin-social img").mouseover(function(e) {
    $(this).attr('src','images/pin-social-icon.png');
	}).mouseout(function(e) {
		$(this).attr('src','images/pin-social-icon2.png');
	});
	
});

$(document).ready(function() {
	$("#privacy-print").click(function(){
		$("#privacy-box-print").printThis();								   
	})		   
});		
$(document).ready(function() {
	$("#terms-print").click(function(){
		$("#terms-box-print").printThis();								   
	})		   
});	

$(document).ready(function(){
   var originalSize = $('.sizer').css('font-size');
  // reset
   $(".resetMe").click(function(){
  $('.sizer').css('font-size', originalSize); 

   });

   // Increase Font Size
   $(".increase").click(function(){
  var currentSize = $('.sizer').css('font-size');
  var currentSize = parseFloat(currentSize)+1;
  $('.sizer').css('font-size', currentSize);

  return false;
   });

   // Decrease Font Size
   $(".decrease").click(function(){
  var currentFontSize = $('.sizer').css('font-size');
  var currentSize = $('.sizer').css('font-size');
  var currentSize = parseFloat(currentSize)-1;
  $('.sizer').css('font-size', currentSize);

  return false;
   });
});




/*$(document).ready(function(){
  $("#slide-1-img").mouseenter(function(){
    $(this).css("display", "none").attr("src","images/slide-2-big.jpg").fadeIn(800);
  });
  $("#slide-1-img").mouseleave(function(){
    $(this).css("display", "none").attr("src","images/slide-1-big.jpg").fadeIn(800);
  });
});*/

/*$(document).ready(function() {
if($(window).outerWidth() >= 480){		

var target = $("#slider-big").offset().top;
$(window).scroll(function() {
    if ($(window).scrollTop() >= target) {
		if($("#menu-button").hasClass('menu-opened')){
			$('#top').css("display", "block");
			$('#navigation').css("position", "relative");
			$('#slider-big, #slider-small').css("margin-top", "0px");
		} else {
			$('#top').css("display", "none");
			$('#navigation').css({"position": "fixed", "z-index": "99"});
			$('#slider-big, #slider-small').css("margin-top", "176px");
		}
    } else if ($(window).scrollTop() <= target) {
		$('#top').css("display", "block");
        $('#navigation').css("position", "relative");
		$('#slider-big, #slider-small').css("margin-top", "0px");
    }
});

}
});	

$(document).ready(function() {
if($(window).outerWidth() <= 479){		

var target = $("#slider-small").offset().top;
$(window).scroll(function() {
    if ($(window).scrollTop() >= target) {
		if($("#menu-button").hasClass('menu-opened')){
			$('#top').css("display", "block");
			$('#navigation').css("position", "relative");
			$('#slider-big, #slider-small').css("margin-top", "0px");
		} else {
			$('#top').css("display", "none");
			$('#navigation').css({"position": "fixed", "z-index": "99"});
			$('#slider-big, #slider-small').css("margin-top", "176px");
		}
    } else if ($(window).scrollTop() <= target) {
		$('#top').css("display", "block");
        $('#navigation').css("position", "relative");
		$('#slider-big, #slider-small').css("margin-top", "0px");
    }
});

}
});	

$(document).ready(function() {
if($(window).outerWidth() >= 480){		

var target = $("#inside-content").offset().top;
$(window).scroll(function() {
    if ($(window).scrollTop() >= target) {
		if($("#menu-button").hasClass('menu-opened')){
			$('#top').css("display", "block");
			$('#navigation').css("position", "relative");
			$('#slider-big, #slider-small').css("margin-top", "0px");
		} else {
			$('#top').css("display", "none");
			$('#navigation').css({"position": "fixed", "z-index": "99"});
			$('#slider-big, #slider-small').css("margin-top", "176px");
		}
    } else if ($(window).scrollTop() <= target) {
		$('#top').css("display", "block");
        $('#navigation').css("position", "relative");
		$('#slider-big, #slider-small').css("margin-top", "0px");
    }
});

}
});	

$(document).ready(function() {
if($(window).outerWidth() <= 479){		

var target = $("#inside-content").offset().top;
$(window).scroll(function() {
    if ($(window).scrollTop() >= target) {
		if($("#menu-button").hasClass('menu-opened')){
			$('#top').css("display", "block");
			$('#navigation').css("position", "relative");
			$('#slider-big, #slider-small').css("margin-top", "0px");
		} else {
			$('#top').css("display", "none");
			$('#navigation').css({"position": "fixed", "z-index": "99"});
			$('#slider-big, #slider-small').css("margin-top", "176px");
		}
    } else if ($(window).scrollTop() <= target) {
		$('#top').css("display", "block");
        $('#navigation').css("position", "relative");
		$('#slider-big, #slider-small').css("margin-top", "0px");
    }
});

}
});	*/


/*$(document).ready(function() {
$(window).scroll(function () {
 if($(window).outerWidth() <= 1190){
	 if($("#menu-button").hasClass("menu-opened")){
		 $("#cssmenu > ul").show();
		 }
 }
});
});	*/

$(document).ready(function() {
	$("#scrolltop").click(function() {
		$("html, body").animate({scrollTop: 0}, 500); 					   
	});					   
});


$(document).ready(function() {
$(function() {
    $('marquee').mouseover(function() {
        $(this).attr('scrollamount',0);
    }).mouseout(function() {
         $(this).attr('scrollamount',3);
    });
});
});

/*$(document).ready(function() {
$(window).scroll(function () {
 if($(window).outerWidth() <= 1125){
 $("#cssmenu ul").hide().removeClass('open');
 }else {
	 $("#cssmenu ul").show();
	 }
});
});	*/

/*$(document).ready(function () {
   var $slides = $("#slider-big img").addClass("gray");

   setTimeout(function() {
       $slides.removeClass("gray");
   }, 800);
});

$(document).ready(function() {
$("#slider-big img").addClass("gray"); 
}); */

/*$(document).ready(function() {
$(window).scroll(function () {
 $("#cssmenu ul").hide().removeClass('open');
});
});	*/

/*$(document).ready(function() {
  $('form').submit(function(e){
    // validation code here
	sweetAlert("Thank you for your mail", "we will get back to you as soon as possible", "success");
  });
});
*/

/*$(document).ready(function() {
	$('#submit').on('click',function(e){
		e.preventDefault();
		var form = $("#contact-form");
		swal({
			title: "Thank you for your mail",
			text: "we will get back to you as soon as possible",
			type: "success"
		});
	});
	$("#ff").on('click',function(e){
	$("#contact-form").submit();									 
	});
});*/

/*$(document).ready(function() {
	$('#submit').on('click',function(e){
		e.preventDefault();
		var form = $("#contact-form");
		swal({
			title: "Thank you for your mail",
			text: "we will get back to you as soon as possible",
			type: "success",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function(isConfirm){
			if (isConfirm) form.submit();
		});
	});
});*/



