<!DOCTYPE html>

<html lang="en-US" >

<head>

		<meta charset="utf-8">

    	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    	<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php $page_title = isset($page_title) ? ' | ' . $page_title : null ?>

    <title><?php echo $this->pref->site_title . $page_title; ?></title>

     <link rel="icon" type="image/png" sizes="32x32" href="<?php echo theme_folder('themeone'); ?>assets/img/favicon.png">

		<link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/css/bootstrap.css">

        <link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/addons/bootstrap/jquery.smartmenus.bootstrap.css">

		<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">

		<link href="https://fonts.googleapis.com/css?family=Engagement" rel="stylesheet">

		<link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/fonts/fonts/stylesheet.css" type="text/css" charset="utf-8" />

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/css/style.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">

             <!--google fonts-->        

        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,800,600,500,700,900" rel="stylesheet">

        <!--google fonts-->   

        <link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>includes/jessore/css/jssor-style.css" />

        <link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/css/animate.css" />

        

        <link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/css/custom.css">

         <?php $CI =& get_instance(); $CI->load->helper('portfolio/portfolio'); ?> 

	</head>

    <?php

	$currentlan=$this->session->userdata('site_lang');

	

	if(empty($currentlan)){

	 $currentlan='english';

	 }

	$actpage=$this->uri->segment(1);

	

	?>

	<body>

		<div class="container">



		<!-- Brand Logo -->

		<div class="col-md-12 brand-logo">

        <a href="<?php echo base_url();?>"><img src="<?php echo $this->pref->site_logo; ?>" alt=""></a></div>

		<!-- Brand Logo -->



		<!-- Navigation -->

		<nav class="navbar navbar-default desktop-only no-marg">

		  <div class="container-fluid no-pad">

		    <!-- Brand and toggle get grouped for better mobile display -->

		    <div class="navbar-header">

		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

		        <span class="sr-only">Toggle navigation</span>

		        <span class="icon-bar"></span>

		        <span class="icon-bar"></span>

		        <span class="icon-bar"></span>

		      </button>

		    </div>

		    <div class="collapse navbar-collapse js-navbar-collapse no-pad" id="bs-example-navbar-collapse-1">      

	    	  <ul class="nav navbar-nav no-pad-mar">

    

	        <!-- Standard dropdown -->

	        	<li class="dropdown <?php if($actpage=='wearemarm'){ echo 'active' ;} ?> "><a href="<?php echo base_url('wearemarm');?>"  role="button" aria-haspopup="true" aria-expanded="false"> <?php echo  get_option_lng('marm_menu_1'); ?></a></li>

	        	<li class="dropdown mega-dropdown">

                <a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_2'); ?></a>

	        	<ul class="dropdown-menu mega-dropdown-menu row">

            <li class="col-sm-12">

              <ul>

                <div class="col-md-3 tab" style="padding:0;">

  <button class="tablinks" onClick="openCity(event, 'incoming')" id="defaultOpen"><?php echo  get_option_lng('marm_submenu_1'); ?></button>

  <button class="tablinks" onClick="openCity(event, 'outbound')"><?php echo  get_option_lng('marm_submenu_2'); ?></button>

  <button class="tablinks" onClick="openCity(event, 'tailor')"><?php echo  get_option_lng('marm_submenu_3'); ?></button>

  <button class="tablinks" onClick="location.href='<?= base_url('hotel');?>'"><?php echo  get_option_lng('marm_submenu_4'); ?></button>

  <button class="tablinks" onClick="location.href='<?= base_url('flight');?>'"><?php echo  get_option_lng('marm_submenu_5'); ?></button>

</div>

<div class="col-md-9" style="padding-left:5px;padding-right:5px;margin-left:auto;margin-right:auto;">

	<div id="incoming" class="tabcontent">

  <?php

$categories_lists_2=get_post_by_cat('turkey');



 if($categories_lists_2){

foreach($categories_lists_2 as $post){

?>

	  <div class="col-md-6 menu-img">

	  	<a href="<?php echo base_url().'tourdetails/index/'.$post->slug?>"><img width="100%" src="<?php echo post_thumb($post,'large'); ?>" alt=""><br />

	  	<p class="menu-text"><?php echo substr(post_title($post),0,14);?></p>		</a>

	  </div>

	

 <?php }} ?>



	</div>



	<div id="outbound" class="tabcontent">

        <?php



$categories_lists_2=other_category_list(3);

 if($categories_lists_2){

foreach($categories_lists_2 as $cat){

?>

	  <div class="col-md-6 menu-img">

	  	<a href="<?php echo base_url().'searchresults/index/'.$cat->slug?>"><img width="100%" src="<?php  echo $cat->image; ?>" alt=""><br />

	  	<p class="menu-text"><?php echo substr(post_title($cat),0,12);?></p>		</a>

	  </div>

	

 <?php }} ?>



	</div>



	<div id="tailor" class="tabcontent">

	

	          <?php



$categories_lists_2=other_category_list(4);

 if($categories_lists_2){

foreach($categories_lists_2 as $cat){

?>

	  <div class="col-md-6 menu-img">

	  	<a href="<?php echo base_url().'searchresults/index/'.$cat->slug?>"><img width="100%" src="<?php  echo $cat->image; ?>" alt=""><br />

	  	<p style="text-align:center;font-weight:600;"><?php echo substr(post_title($cat),0,12);?></p>		</a>

	  </div>

	

 <?php }} ?>

	</div>

	<div id="hotel" class="tabcontent">

		Coming Soon

	</div>

	<div id="flight" class="tabcontent">

		Coming Soon

	</div>

</div>

              </ul>

            </li>

          </ul>

          </li>

	        	<li class="dropdown <?php if($actpage=='marmxclusive'){ echo 'active' ;} ?> "><a href="<?php echo base_url('marmxclusive');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_3'); ?></span></a></li>

	        	<li class="dropdown  <?php if($actpage=='marmcorporate'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmcorporate');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_4'); ?></a></li>

	        	<li class="dropdown <?php if($actpage=='marmmice'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmmice');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_5'); ?></a></li>

	        	

        

   <li class="dropdown  <?php if($actpage=='marmwellness'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmwellness');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_7'); ?></a></li>

  <li class="dropdown  <?php if($actpage=='marmgolf'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmgolf');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_8'); ?></a></li>

  <li class="dropdown  <?php if($actpage=='marmweddings'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmweddings');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_9'); ?></a></li> 

        <li class="dropdown <?php if($actpage=='contact'){ echo 'active' ;} ?>"><a href="<?php echo base_url('contact');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_6'); ?></a></li>

			      </ul>

			      

			      <!-- .navbar-right -->

			      <ul class="nav navbar-nav navbar-right">

			        	<li class="dropdown dropdown-cols-2">

                        	<a> <form class="search-form" role="search"  action="<?php echo base_url().'searchresults/index'?>">

        <div class="form-group pull-right" id="search">

          <input type="text" class="form-control" placeholder="search tours" name="search">

          <button type="submit" class="form-control form-control-submit">Submit</button>

          <span class="search-label"><i class="fa fa-search" aria-hidden="true"></i></span>

        </div>

      </form></a>

			        		

			        	</li>

			        	

                      

                       <?php /*?>  <?php foreach(list_langs() as $key=>$lang): ?>

                        

                        <?php

					

					

						 if($key==1){ ?>

                      <li class="dropdown dropdown-cols-2 language" >

			        		<a style="padding:8px 1px; padding-left:5px;" <?php if($currentlan=='turkish'){ ?> class="active" <?php } ?> href="<?=base_url('langswitch/switchLanguage/turkish')?>" role="button" aria-haspopup="true" aria-expanded="false"><img  id="lang-turkey"></a>

                            </li>

                            <?php }else{ ?>

                             <li class="dropdown dropdown-cols-2 language">

                            	

         <a  style="padding:8px 1px;" <?php if($currentlan=='english'){ ?> class="active" <?php } ?> href="<?=base_url('langswitch/switchLanguage/english')?>" role="button" aria-haspopup="true" aria-expanded="false">

         <img  id="lang-eng"></a> </li>

                            <?php } ?>

			        	

                       <?php endforeach; ?><?php */?>

			        

			        </ul>

			      <!-- /.navbar-right -->

			      

			    </div>

			  </div>

			</nav>

			<!-- Navigation -->

            

	<!-- Navigation Mobile -->

		<!-- Navbar -->

      <div class="navbar navbar-default mobile-only mobile-m-block" role="navigation">

        <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

            <span class="sr-only">Toggle navigation</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

          </button>

        </div>

        <div class="navbar-collapse collapse">



          <!-- Left nav -->

          <ul class="nav navbar-nav">

           

            <li class="<?php if($actpage=='wearemarm'){ echo 'active' ;} ?>"><a href="<?php echo base_url('wearemarm');?>"  role="button" aria-haspopup="true" aria-expanded="false"> <?php echo  get_option_lng('marm_menu_1'); ?></a></li>

            <li><a href="#"><?php echo  get_option_lng('marm_menu_2'); ?> <span class="caret"></span></a>

              <ul class="dropdown-menu">

                <li><a href="#"><?php echo  get_option_lng('marm_submenu_1'); ?> <span class="caret"></span></a>

                  <ul class="dropdown-menu">

                <?php

$categories_lists_2=get_post_by_cat('turkey');



 if($categories_lists_2){

foreach($categories_lists_2 as $post){

?>

	  <li>

	  	<a href="<?php echo base_url().'tourdetails/index/'.$post->slug?>"><?php echo substr(post_title($post),0,14);?></a>

	 </li>
	  

	

 <?php }} ?>                   



                  </ul>

                </li>

                <li><a href="#"><?php echo  get_option_lng('marm_submenu_2'); ?> <span class="caret"></span></a>

                  <ul class="dropdown-menu">

             

 <?php



$categories_lists_2=other_category_list(3);

 if($categories_lists_2){

foreach($categories_lists_2 as $cat){

?>

	  <li>

	  	<a href="<?php echo base_url().'searchresults/index/'.$cat->slug?>"><?php  echo $cat->title; ?></a>

	 </li>

	

 <?php }} ?>

                  </ul>

                </li>

                <li><a href="#"><?php echo  get_option_lng('marm_submenu_3'); ?> <span class="caret"></span></a>

                  <ul class="dropdown-menu">

                                

 <?php



$categories_lists_2=other_category_list(4);

 if($categories_lists_2){

foreach($categories_lists_2 as $cat){

?>

	  <li>

	  	<a href="<?php echo base_url().'searchresults/index/'.$cat->slug?>"><?php  echo $cat->title; ?></a>

	 </li>

	

 <?php }} ?>

                  </ul>

                </li>

                <li><a href="<?= base_url('hotel');?>"><?php echo  get_option_lng('marm_submenu_4'); ?> </a>

                  

                </li>

                <li><a href="<?= base_url('flight');?>"><?php echo  get_option_lng('marm_submenu_5'); ?> </a>

                  

                </li>

              </ul>

            </li>

            </ul>

            <ul class="nav navbar-nav navbar-right">

                       <li class=" <?php if($actpage=='marmxclusive'){ echo 'active' ;} ?> "><a href="<?php echo base_url('marmxclusive');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_3'); ?></span></a></li>

	        	<li class="  <?php if($actpage=='marmcorporate'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmcorporate');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_4'); ?></a></li>

	        	<li class=" <?php if($actpage=='marmmice'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmmice');?>" class="" role="button" aria-haspopup="true" aria-expanded="false"><?php echo  get_option_lng('marm_menu_5'); ?></a></li>

                 <li class="  <?php if($actpage=='marmwellness'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmwellness');?>" class="" aria-haspopup="true" aria-expanded="false">Marm <span>Wellness</span></a></li>

  <li class="  <?php if($actpage=='marmgolf'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmgolf');?>" >Marm <span>Golf</span></a></li>

  <li class="  <?php if($actpage=='marmweddings'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmweddings');?>" >Marm <span>Weddings</span></a></li> 

	        	<li class=" <?php if($actpage=='contact'){ echo 'active' ;} ?>"><a href="<?php echo base_url('contact');?>"><?php echo  get_option_lng('marm_menu_6'); ?></a></li>

        	<li>

			    <a>

			    	<form class="navbar-form" role="search" action="<?php echo base_url().'searchresults/index'?>">

        <div class="input-group">

            <input type="text" class="form-control" name="search" placeholder="Search">

            <div class="input-group-btn">

                <button class="btn btn-default" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>

            </div>

        </div>

        </form>

        

			    </a>

			</li>

			

		

            

             <?php /*?>    <?php foreach(list_langs() as $key=>$lang): ?>

                        

                        <?php

					

					

						 if($key==1){ ?>

                      

                            	<li>

			   <a <?php if($currentlan=='turkish'){ ?> class="active" <?php } ?> href="<?=base_url('langswitch/switchLanguage/'.$lang)?>" ><img  id="lang-turkey"></a>

			</li>

                            <?php }else{ ?>

                            <li style="float:left;" class="language">

			     <a   <?php if($currentlan=='english'){ ?> class="active" <?php } ?> href="<?=base_url('langswitch/switchLanguage/'.$lang)?>" >

         <img  id="lang-eng"></a>

			</li>

                             

                            <?php } ?>

			        	

                       <?php endforeach; ?><?php */?>

          </ul>



        </div><!--/.nav-collapse -->

      </div>



			<!-- Navigation Mobile -->



          