<!DOCTYPE html>
<html lang="en-US" >
<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Marm Tour</title>
		<link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/addons/bootstrap/jquery.smartmenus.bootstrap.css">
		<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Engagement" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/fonts/fonts/stylesheet.css" type="text/css" charset="utf-8" />
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
        <link rel="stylesheet" href="<?php echo theme_folder('themeone'); ?>assets/css/custom.css">
         <?php $CI =& get_instance(); $CI->load->helper('portfolio/portfolio'); ?> 
	</head>
    <?php
	$currentlan=$this->session->userdata('site_lang');
	
	if(empty($currentlan)){
	 $currentlan='english';
	 }
	$actpage=$this->uri->segment(1);
	?>
	<body>
		<div class="container">

		<!-- Brand Logo -->
		<div class="col-md-12 brand-logo">
        <a href="<?php echo base_url();?>"><img src="<?php echo $this->pref->site_logo; ?>" alt=""></a></div>
		<!-- Brand Logo -->

		<!-- Navigation -->
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		    </div>
		    <div class="collapse navbar-collapse js-navbar-collapse" id="bs-example-navbar-collapse-1">      
	    	  <ul class="nav navbar-nav">
    
	        <!-- Standard dropdown -->
	        	<li class="dropdown <?php if($actpage=='wearemarm'){ echo 'active' ;} ?> "><a href="<?php echo base_url('wearemarm');?>"  role="button" aria-haspopup="true" aria-expanded="false">we are <span>marm</span></a></li>
	        	<li class="dropdown mega-dropdown">
                <a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">services</a>
	        	<ul class="dropdown-menu mega-dropdown-menu row">
            <li class="col-sm-12">
              <ul>
                <div class="col-md-3 tab" style="padding:0;">
  <button class="tablinks" onclick="openCity(event, 'incoming')" id="defaultOpen">incoming</button>
  <button class="tablinks" onclick="openCity(event, 'outbound')">outbound</button>
  <button class="tablinks" onclick="openCity(event, 'tailor')">Tailor made</button>
  <button class="tablinks" onclick="openCity(event, 'hotel')">hotel</button>
  <button class="tablinks" onclick="openCity(event, 'flight')">flight</button>
</div>
<div class="col-md-9" style="padding-left:5px;padding-right:5px;margin-left:auto;margin-right:auto;">
	<div id="incoming" class="tabcontent">
	  <div class="col-md-6">
	  	<a href="<?php echo base_url().'searchresults/index/istanbul'?>"><img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Istanbul</p>		</a>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Cappadocia</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Izmir</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Antalya</p>
	  </div>
	</div>

	<div id="outbound" class="tabcontent">
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Europe</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Asia</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Africa</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">America</p>
	  </div>
	</div>

	<div id="tailor" class="tabcontent">
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Honeymoon</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Adventure</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">Festival</p>
	  </div>
	  <div class="col-md-6">
	  	<img width="100%" src="<?php echo theme_folder('themeone'); ?>assets/img/post1.jpg" alt=""><br />
	  	<p style="text-align:center;font-weight:600;">History</p>
	  </div>
	</div>
	<div id="hotel" class="tabcontent">
		Coming Soon
	</div>
	<div id="flight" class="tabcontent">
		Coming Soon
	</div>
</div>
              </ul>
            </li>
          </ul>
          </li>
	        	<li class="dropdown <?php if($actpage=='marmxclusive'){ echo 'active' ;} ?> "><a href="<?php echo base_url('marmxclusive');?>" class="" role="button" aria-haspopup="true" aria-expanded="false">marm <span>xclusive</span></a></li>
	        	<li class="dropdown  <?php if($actpage=='marmcorporate'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmcorporate');?>" class="" role="button" aria-haspopup="true" aria-expanded="false">marm <span>corporate</span></a></li>
	        	<li class="dropdown <?php if($actpage=='marmmice'){ echo 'active' ;} ?>"><a href="<?php echo base_url('marmmice');?>" class="" role="button" aria-haspopup="true" aria-expanded="false">mice</a></li>
	        	<li class="dropdown <?php if($actpage=='contact'){ echo 'active' ;} ?>"><a href="<?php echo base_url('contact');?>" class="" role="button" aria-haspopup="true" aria-expanded="false">contact</a></li>
        
        <!--
        <li class="dropdown dropdown-cols-2">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Two-cols dropdown <span class="caret"></span></a>                    
          <div class="dropdown-menu">
            <div>
              <h5>Link group 1</h5>
              <ul>
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </div>
            <div>
              <h5>Link group 2</h5>
              <ul>
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </div>
          </div>
        </li>
         -->       
        
			      </ul>
			      
			      <!-- .navbar-right -->
			      <ul class="nav navbar-nav navbar-right">
			        	<li class="dropdown dropdown-cols-2">
			        		<a> <form class="search-form" action="<?php echo base_url().'searchresults/index'?>" role="search">
        <div class="form-group pull-right" id="search">
          <input type="text" class="form-control" name="search" placeholder="Search">
          <button type="submit" class="form-control form-control-submit">Submit</button>
          <span class="search-label"><i class="fa fa-search" aria-hidden="true"></i></span>
        </div>
      </form></a>
			        	</li>
			        	
                      
                         <?php foreach(list_langs() as $key=>$lang): ?>
                        
                        <?php
					
					
						 if($key==1){ ?>
                      <li class="dropdown dropdown-cols-2 language" style="margin-left: -26px;">
			        		<a <?php if($currentlan=='turkish'){ ?> class="active" <?php } ?> href="<?=base_url('langswitch/switchLanguage/'.$lang)?>" role="button" aria-haspopup="true" aria-expanded="false"><img  id="lang-turkey"></a>
                            </li>
                            <?php }else{ ?>
                             <li class="dropdown dropdown-cols-2 language">
                            	
         <a <?php if($currentlan=='english'){ ?> class="active" <?php } ?> href="<?=base_url('langswitch/switchLanguage/'.$lang)?>" role="button" aria-haspopup="true" aria-expanded="false">
         <img  id="lang-eng"></a> </li>
                            <?php } ?>
			        	
                       <?php endforeach; ?>
			        
			        </ul>
			      <!-- /.navbar-right -->
			      
			    </div>
			  </div>
			</nav>
			<!-- Navigation -->
          