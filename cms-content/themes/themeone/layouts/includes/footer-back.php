</div>
	<!-- Newsletter -->
			<div class="container bg-newsletter">
				<div class="newsletter">
					<h2><?= get_option_lng('subscribe_title')?></h2>
					<input type="text" placeholder="<?= get_option_lng('subscribe_Placeholder')?>">
					<input type="submit" value="<?= get_option_lng('subscribe_button')?>">
				</div>
			</div>
		<!-- Newsletter -->

<!-- Footer -->
			<div class="container footer">
				<div class="col-md-3">
					<img src="<?php echo theme_folder('themeone'); ?>assets/img/logo.png" alt="">
					<br /><br />
					<p><?= get_option_lng('footer_main_content')?></p>
					<p>
					<?= get_option('footer_address_content')?>
					<span class="redspan">P:</span> <?= get_option('footer_phone')?> <br />
					<span class="redspan">E:</span> <?= get_option('footer_email')?></p>
					<p class="footer-social">
						<a href=""><i class="fab fa-facebook-f"></i></a>
						<a href=""><i class="fab fa-twitter"></i></a>
						<a href=""><i class="fab fa-youtube"></i></a>
						<a href=""><i class="fab fa-pinterest-p"></i></a>
					</p>
				</div>
				<div class="col-md-3">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12743709.793692345!2d26.17583854238866!3d38.75780278076298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b0155c964f2671%3A0x40d9dbd42a625f2a!2sTurkey!5e0!3m2!1sen!2sin!4v1539546041914" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="col-md-3">
					<h2 class="redspan">Links</h2>
					<ul class="footer_link">
						<li><a href="<?php echo base_url('wearemarm');?>">WE ARE <span class="redspan">MARM</span></a></li>
						<li><a href="<?php echo base_url('services');?>">SERVICES</a></li>
						<li><a href="<?php echo base_url('marmxclusive');?>">MARM XCLUSIVE</a></li>
						<li><a href="<?php echo base_url('marmcorporate');?>">MARM CORPORATE</a></li>
						<li><a href="<?php echo base_url('contact');?>">CONTACT</a></li>
                        <li><a href="<?php echo base_url('term');?>">TERMS AND PRIVACY</a></li>
					</ul>
				</div>
					<div class="row justify-content-center">
				<div class="col-md-3">
				<div class="row lightbox">
					<h2 class="redspan">Gallery</h2>
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1200px-Hopetoun_falls.jpg" class="col-md-6 img-fluid hover-shadow cursor" style="padding-right: 5px;
    padding-left: 5px;" onclick="openModal();currentSlide(1)">
                <img src="https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/06/edinburgh_meadows_2008_middle_meadow_walk_by_catharine_ward_thompson.jpg?itok=ysmDaSjD&fc=50,50" class="col-md-6 img-fluid hover-shadow cursor" style="padding-right: 5px;
    padding-left: 5px;" onclick="openModal();currentSlide(2)">
        </div>
        <div class="row lightbox">
            
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1200px-Hopetoun_falls.jpg" class="col-md-6 img-fluid hover-shadow cursor" style="padding-right: 5px;
    padding-left: 5px;" onclick="openModal();currentSlide(3)">
                <img src="https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/06/edinburgh_meadows_2008_middle_meadow_walk_by_catharine_ward_thompson.jpg?itok=ysmDaSjD&fc=50,50" class="col-md-6 img-fluid hover-shadow cursor" style="padding-right: 5px;
    padding-left: 5px;" onclick="openModal();currentSlide(4)">
        </div>
    </div>
			</div>
		<!-- Footer -->
<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 4</div>
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1200px-Hopetoun_falls.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 4</div>
      <img src="https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/06/edinburgh_meadows_2008_middle_meadow_walk_by_catharine_ward_thompson.jpg?itok=ysmDaSjD&fc=50,50">
    </div>

    <div class="mySlides">
      <div class="numbertext">3 / 4</div>
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1200px-Hopetoun_falls.jpg" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">4 / 4</div>
      <img src="https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/06/edinburgh_meadows_2008_middle_meadow_walk_by_catharine_ward_thompson.jpg?itok=ysmDaSjD&fc=50,50" style="width:100%">
    </div>
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>


    <div class="column">
      <img class="demo cursor" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1200px-Hopetoun_falls.jpg" style="width:100%" onclick="currentSlide(1)" alt="Nature and sunrise">
    </div>
    <div class="column">
      <img class="demo cursor" src="https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/06/edinburgh_meadows_2008_middle_meadow_walk_by_catharine_ward_thompson.jpg?itok=ysmDaSjD&fc=50,50" style="width:100%" onclick="currentSlide(2)" alt="Snow">
    </div>
    <div class="column">
      <img class="demo cursor" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1200px-Hopetoun_falls.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
    </div>
    <div class="column">
      <img class="demo cursor" src="https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/06/edinburgh_meadows_2008_middle_meadow_walk_by_catharine_ward_thompson.jpg?itok=ysmDaSjD&fc=50,50" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
    </div>
  </div>
</div>
</div>
		<!-- Copyright -->
			<div align="middle" style="padding:20px;" class="container">
				<p>All Rights Reserved | Marm Travel | 2018</p>
			</div>
		<!-- Copyright -->
<div id="cookieConsent">
		    <div id="closeCookieConsent">x</div>
		    This website is using cookies. <a href="#" target="_blank">More info</a>. <a class="cookieConsentOK">That's Fine</a>
		</div>

		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="<?php echo theme_folder('themeone'); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo theme_folder('themeone'); ?>assets/js/jquery.smartmenus.js"></script>
		<script src="<?php echo theme_folder('themeone'); ?>assets/addons/bootstrap/jquery.smartmenus.bootstrap.js"></script>
        <script src="<?php echo theme_folder('themeone'); ?>assets/js/lightbox.js"></script>
         <script src="<?php echo theme_folder('themeone'); ?>assets/js/jquery.cookie.js"></script>
        	<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
		<script>
			$(document).ready(function(){
  $('#search').on("click",(function(e){
  $(".form-group").addClass("sb-search-open");
    e.stopPropagation()
  }));
   $(document).on("click", function(e) {
    if ($(e.target).is("#search") === false && $(".form-control").val().length == 0) {
      $(".form-group").removeClass("sb-search-open");
    }
  });
    $(".form-control-submit").click(function(e){
      $(".form-control").each(function(){
        if($(".form-control").val().length == 0){
          e.preventDefault();
          $(this).css('border', '2px solid red');
        }
    })
  })
})
			//jQuery(document).on('hover', '.mega-dropdown', function(e) {
  //e.stopPropagation()
//})

			function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
		</script>
        
            <script>
			
$(".cookieConsentOK, closeCookieConsent ").click(function(){
$('#cookieConsent').hide();
    $.post("<?php echo base_url(''); ?>",
    { 
        name: "Donald Duck",
        city: "Duckburg"
    },
    function(data, status){
     
    });

});
//------------------------
			jQuery('#flipFlop').modal();
			</script>
            <script>
		
if(!$.cookie("setpopup")){
$('#flipFlop22').modal();	
	}	
$("#sethomepopup").click(function(){
$.cookie("setpopup", 1);

});
//---------------------
if($.cookie("setcookielaw")==1){
$('#cookieConsent').hide();	
	}

$(".cookieConsentOK, #closeCookieConsent ").click(function(){
	$('#cookieConsent').hide();
	$.cookie("setcookielaw", 1);
	});
//------------------------
			jQuery('#flipFlop').modal();
			</script>

	</body>
</html>