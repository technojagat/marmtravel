</div>
	<!-- Newsletter -->
			<div class="container bg-newsletter">
				<div class="newsletter">
					<h2><?= lang('subscribe_title')?></h2>
                    <form id="form_subscribe">
					<input type="email" id="subscriber_email" placeholder="<?= lang('subscribe_placeholder')?>" required>
					<input type="submit" onClick="submit_subscribe()" value="<?= lang('subscribe_button')?>">
                    <i id="submit_subscribespin" class="fa fa-spinner fa-spin" style="display:none;" ></i>
                    </form>
 <div class="alert alert-success" style="display:none;"  id="successMessage">
 <?= lang('success_email_msg');?>
  </div>
				</div>
			</div>
		<!-- Newsletter -->
        <?php $actpage=$this->uri->segment(1);
		
		if(empty($actpage) || $actpage=='home'){?>
	<!-- Three Blocks -->

			<div class="container block-panel">

				<div class="col-md-4 features">

					<h1>

					

					<?= get_option_lng('marm_feature_title')?></h1>
                    <a href="<?php echo base_url('feature');?>">
					<img src="<?php echo theme_folder('themeone'); ?>assets/img/features-icon.png" alt="">
                    </a>

					<p><?= get_option_lng('feature_content')?></p>

				</div>

				<div class="col-md-4 features">

					<h1><?= get_option_lng('marm_network_title')?></h1>
 <a href="<?php echo base_url('networkpartners');?>">
					<img src="<?php echo theme_folder('themeone'); ?>assets/img/network-icon.png" alt="">
                    </a>

					<p><?= get_option_lng('network_content')?></p>

				</div>

				<div class="col-md-4 features">

					<h1><?= get_option_lng('marm_help_title')?></h1>
 <a href="<?php echo base_url('helpyou');?>">
					<img src="<?php echo theme_folder('themeone'); ?>assets/img/help-icon.png" alt="">
                    </a>

					<p><?= get_option_lng('help_content')?></p>

				</div>

			</div>

		<!-- Three Blocks -->
        <?php } ?>
<!-- Footer -->
			<div class="container footer">
				<div class="col-md-3">
					<img src="<?php echo theme_folder('themeone'); ?>assets/img/logo.png" alt="">
					<br /><br />
					<p><?= get_option_lng('footer_main_content')?></p>
					<p>
					<?= get_option('footer_address_content')?>
					<span class="redspan">P:</span> <?= get_option('footer_phone')?> <br />
                    <span class="redspan">F:</span> +90 216 560 0707   <br />
					<span class="redspan">E:</span> <?= get_option('footer_email')?></p>
					<p class="footer-social">
						<a href="https://www.facebook.com/marmtravel/"><i class="fab fa-facebook-f"></i></a>
						<a href="https://www.instagram.com/marm_travel/"><i class="fab fa-instagram"></i></a>
						<!--<a href=""><i class="fab fa-youtube"></i></a>
						<a href=""><i class="fab fa-pinterest-p"></i></a>-->
					</p>
				</div>
				<div class="col-md-3">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12743709.793692345!2d26.17583854238866!3d38.75780278076298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b0155c964f2671%3A0x40d9dbd42a625f2a!2sTurkey!5e0!3m2!1sen!2sin!4v1539546041914" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="col-md-3">
					<h2 class="redspan"><?php echo  lang('links_title'); ?></h2>
					<ul class="footer_link">
						<!--li><a href="<?php echo base_url('wearemarm');?>">WE ARE <span class="redspan">MARM</span></a></li-->
            <li><a href="<?php echo base_url('wearemarm');?>"><?php echo  get_option_lng('marm_menu_1'); ?></a></li>
						<?php /*?><li><a href="<?php echo base_url('services');?>"><?php echo  get_option_lng('marm_menu_2'); ?></a></li><?php */?>
						<li><a href="<?php echo base_url('marmxclusive');?>"><?php echo  get_option_lng('marm_menu_3'); ?></a></li>
						<li><a href="<?php echo base_url('marmcorporate');?>"><?php echo  get_option_lng('marm_menu_4'); ?></a></li>
						<li><a href="<?php echo base_url('marmmice');?>"><?php echo  get_option_lng('marm_menu_5'); ?></a></li>
            <li ><a href="<?php echo base_url('marmwellness');?>" ><?php echo  get_option_lng('marm_menu_7'); ?></a></li>
            <li><a href="<?php echo base_url('marmgolf');?>" ><?php echo  get_option_lng('marm_menu_8'); ?></a></li>
            <li><a href="<?php echo base_url('marmweddings');?>" ><?php echo  get_option_lng('marm_menu_9'); ?></a></li> 
            <li><a href="<?php echo base_url('term');?>"><?= strtoupper(get_option_lng('marm_terms_privacy'))?></a></li>
                    
   
					</ul>
				</div>
					<div class="row justify-content-center">
				<div class="col-md-3">
				<div class="row lightbox footerslider">
					<h2 class="redspan"><?php echo  lang('gallery_title'); ?></h2>
            <?php

$footerslider=get_slider("footer_gallery");

?>
     <?php

                  if(!empty($footerslider)){

					  foreach( $footerslider as $sl){

				  ?>

				    

<div class="col-lg-6 col-md-6 col-xs-12 thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo $sl['background']; ?>"
                   data-target="#image-gallery">
                    <img class="img-thumbnail"
                         src="<?php echo $sl['background']; ?>"
                         alt="Footer Slidert">
                </a>
            </div>

				    

                    <?php }} ?>
        </div>
        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
			</div>
		<!-- Footer -->

</div>
		<!-- Copyright -->
			<div align="middle" style="padding:20px;" class="container">
				<p>All Rights Reserved | Marm Travel | 2018</p>
			</div>
		<!-- Copyright -->
<div id="cookieConsent">
		    <div id="closeCookieConsent">x</div>
		    <?php echo get_option_lng('cookie_policy_content'); ?>
		</div>

		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="<?php echo theme_folder('themeone'); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo theme_folder('themeone'); ?>assets/js/jquery.smartmenus.js"></script>
		<script src="<?php echo theme_folder('themeone'); ?>assets/addons/bootstrap/jquery.smartmenus.bootstrap.js"></script>
        <script src="<?php echo theme_folder('themeone'); ?>assets/js/lightbox.js"></script>
         <script src="<?php echo theme_folder('themeone'); ?>assets/js/jquery.cookie.js"></script>
        	<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
         <script type="text/javascript" src="<?php echo theme_folder('themeone'); ?>includes/jessore/js/jssor.slider.min.js"></script>
		<script type="text/javascript" src="<?php echo theme_folder('themeone'); ?>includes/jessore/js/jssor1-script.js"></script>
		<script type="text/javascript" src="<?php echo theme_folder('themeone'); ?>includes/jessore/js/jssor2-script.js"></script>
		<script type="text/javascript" src="<?php echo theme_folder('themeone'); ?>includes/jessore/js/jssor3-script.js"></script>
        <script>
        jssor_1_slider_init();
		jssor_2_slider_init();
		jssor_3_slider_init();
		jssor_4_slider_init();
</script>
<script>
$(document).ready(function () {
	$("#menu-opt-home").addClass("active-page");
});
</script>
<script src="<?php echo theme_folder('themeone'); ?>assets/js/script.js" type="text/javascript"></script>
		<script>
			$(document).ready(function(){
  $('#search').on("click",(function(e){
  $(".form-group").addClass("sb-search-open");
    e.stopPropagation()
  }));
   $(document).on("click", function(e) {
    if ($(e.target).is("#search") === false && $(".form-control").val().length == 0) {
      $(".form-group").removeClass("sb-search-open");
    }
  });
    $(".form-control-submit").click(function(e){
      $(".form-control").each(function(){
        if($(".form-control").val().length == 0){
          e.preventDefault();
          $(this).css('border', '2px solid red');
        }
    })
  })
})
			//jQuery(document).on('hover', '.mega-dropdown', function(e) {
  //e.stopPropagation()
//})

			function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
		</script>
        
            <script>
			
$(".cookieConsentOK, closeCookieConsent ").click(function(){
$('#cookieConsent').hide();
    $.post("<?php echo base_url(''); ?>",
    { 
        name: "Donald Duck",
        city: "Duckburg"
    },
    function(data, status){
     
    });

});
//------------------------
			jQuery('#flipFlop').modal();
			</script>
            <script>
		
if(!$.cookie("setpopup")){
$('#flipFlop22').modal('hide');	
	}	
$("#sethomepopup").click(function(){
$.cookie("setpopup", 1);

});
//---------------------
if($.cookie("setcookielaw")==1){
$('#cookieConsent').hide();	
	}

$(".cookieConsentOK, #closeCookieConsent ").click(function(){
	$('#cookieConsent').hide();
	$.cookie("setcookielaw", 1);
	});
//------------------------
			jQuery('#flipFlop').modal();
			</script>
            <script>
			let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

			</script>
            <script>
function popupeventHome(){
	 $("#popupeventspin").show();
	
	var subemail=$('#poupemail').val();
   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/sendsubscribe'); ?>',
        data: {data: subemail},
        success: function(result) {
			 $("#popupeventspin").hide();
			 $("#popupeventsuccess").show();
			 setTimeout(function() {
        $("#popupeventsuccess").hide();
    }, 3000);
         
        }
    });


}	
	
$("#form_subscribe").on("submit", function (e) {
    e.preventDefault();
	 $("#submit_subscribespin").show();
	
	var subemail=$('#subscriber_email').val();
   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/sendsubscribe'); ?>',
        data: {data: subemail},
        success: function(result) {
			 $("#submit_subscribespin").hide();
			 $("#successMessage").show();
			 setTimeout(function() {
        $("#successMessage").hide();
    }, 3000);
         
        }
    });


});

$("#subscribe_tour").on("submit", function (e) {
	 e.preventDefault();
	$("#submit_subscribe_tourspin").show();
	
	var subemail=$('#submit_subscribe_tour').val();
	var refurl=$('#subscribe_tour_refurl').val();
	
   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/sendsubscribetour'); ?>',
        data: {email:subemail,refurl:refurl},
        success: function(result) {
			$("#submit_subscribe_tourspin").hide();
			 $("#submit_subscribe_tour2").show();
			 setTimeout(function() {
		
        $("#submit_subscribe_tour2").hide();
    }, 3000);
         
        }
    });


});

function submit_sendafriend(){
	 $("#successMessage2spin").show();
	
	var subemail=$('#subscriber_email').val();
   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/sendafriend'); ?>',
        data: {data: $("#sendfriend_form").serialize()},
        success: function(result) {
			$("#successMessage2spin").hide();
			 $("#successMessage2").show();
			setTimeout(function(){ 
			 
			 $("#successMessage2").hide(); 
			 }, 3000);
	
       
   
         
        }
    });


}


$("#marmcorporate_form").on("submit", function (e) {
    e.preventDefault();
	 $("#submit_marmcorporatespin").show();
	
	var subemail=$('#subscriber_email').val();
   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/marmcorporate'); ?>',
        data: {data: $("#marmcorporate_form").serialize()},
        success: function(result) {
			$("#submit_marmcorporatespin").hide();
			 $("#submit_marmcorporate").show();
			setTimeout(function(){ 
			 
			 $("#submit_marmcorporate").hide(); 
			 }, 3000);
	
       
   
         
        }
    });


});
//====================
$("#hotel_form").on("submit", function (e) {
    e.preventDefault();
	 $("#hotel_spin").show();

   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/hotel'); ?>',
        data: {data: $("#hotel_form").serialize()},
        success: function(result) {
			$("#hotel_spin").hide();
			 $("#hotel_success").show();
			setTimeout(function(){ 
			 
			 $("#hotel_success").hide(); 
			 }, 3000);
	
       
   
         
        }
    });


});
//====================
$("#flight_form").on("submit", function (e) {
    e.preventDefault();
	 $("#flight_spin").show();

   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/flight'); ?>',
        data: {data: $("#flight_form").serialize()},
        success: function(result) {
			$("#flight_spin").hide();
			 $("#flight_success").show();
			setTimeout(function(){ 
			 
			 $("#flight_success").hide(); 
			 }, 3000);
	
       
   
         
        }
    });


});
//===================
$("#helpyou_form").on("submit", function (e) {
    e.preventDefault();
	 $("#submit_marmcorporatespin").show();
	
	var subemail=$('#subscriber_email').val();
   $.ajax({
        type: "POST",
        url: '<?php echo base_url('home/helpyou'); ?>',
        data: {data: $("#helpyou_form").serialize()},
        success: function(result) {
			$("#submit_marmcorporatespin").hide();
			 $("#submit_marmcorporate").show();
			setTimeout(function(){ 
			 
			 $("#submit_marmcorporate").hide(); 
			 }, 3000);
	
       
   
         
        }
    });


});
			</script>

	</body>
</html>