	<!-- Cover Image -->
			<div class="col-md-12 tour-cover">
				<img src="<?php echo get_option('marmmice_image_background'); ?>" alt="Turkey Cover">
				<?php /*?><p><?php echo get_option_lng('marmcontact_title'); ?></p><?php */?>
			</div>
			<!-- Cover Image -->
			<div class="col-md-3 side-panel right">
	<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>
			</div>
			<div class="col-md-9" style="padding:0;">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12743709.793692345!2d26.17583854238866!3d38.75780278076298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b0155c964f2671%3A0x40d9dbd42a625f2a!2sTurkey!5e0!3m2!1sen!2sin!4v1539546041914" width="100%" height="340" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		<div class="container  height-contact-details">
			<div class="col-md-4 contact-details contact-details-border-right">
				<h1><?php echo get_option_lng('marm_heading1'); ?></h1>

				<p><?php echo get_option('marm_heading1_content'); ?> </p>
			</div>
			<div class="col-md-3 help-desk">
				<h1><?php echo get_option_lng('marm_heading2'); ?></h1>
				<p><?php echo get_option('marm_heading2_content'); ?> </p>
			</div>
			<div class="col-md-3 help-desk" style="border-right:0;">
				<h1><?php echo get_option_lng('marm_heading3'); ?> </h1>
				<p><?php echo get_option('marm_heading3_content'); ?></p>
			</div>
		</div>