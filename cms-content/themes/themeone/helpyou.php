





			<!-- Cover Image -->

			<div class="col-md-12 no-pad pad-left turkey-cover slider-eff">

				<img src="<?php echo get_option('help_image_background'); ?>" alt="Let's Help You">

				<div class="caption-inside"><?= get_option_lng('marm_help_title');?></div>

			</div>

			<!-- Cover Image -->

			<div class="col-md-12 no-marg no-pad turkey-bg-n">

			<div class="col-md-3 side-panel right turkey-margin caption-nav nav-right-border">

		<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>

			</div>

			<div class="col-md-9 marm-mice-desc">

				<br><br><p><?= get_option_lng('help_content')?></p>	



				<div class="requirement-form">

					<h1><?= lang('helpform_head');?></h1>

					<form id="helpyou_form" role="form" class="form-horizontal">

                     <input type="hidden" name="subject" value="Help You" >

					    <label class="control-label col-sm-4" for="company-name"><?= lang('namesurname');?>:</label>

					    <input type="text" class="form-control" id="company-name"  name="company-name" placeholder="<?= lang('namesurname');?>" required>

					    <label class="control-label col-sm-4" for="contact-name"><?= lang('email');?>:</label>

					    <input type="text" class="form-control" id="contact-name" name="email" placeholder="<?= lang('email');?>" required>

					    <label class="control-label col-sm-4" for="designation"><?= lang('phonenum');?>:</label>

					    <input type="text" class="form-control" id="designation" name="designation" placeholder="<?= lang('phonenum');?>" required>

					    <label class="control-label col-sm-4" for="query"><?= lang('message');?>:</label>

					    <textarea id="query" name="query" placeholder="<?= lang('message');?>" required></textarea>

						

					    <div style="margin: 0 auto;width: 100%;text-align: center;">

                         <div class="alert alert-success" style="display:none;"  id="submit_marmcorporate">

						 <?= lang('success_form_msg');?>

 						 </div>

					    	<input type="submit" name="submit"  value="<?= lang('send_button');?>">

                             <i id="submit_marmcorporatespin" class="fa fa-spinner fa-spin" style="display:none;font-size:24px;" ></i>

						</div>



					</form>

				</div>

			</div>
            
            </div>

	