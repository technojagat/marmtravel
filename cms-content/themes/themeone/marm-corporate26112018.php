
			<!-- Cover Image -->
			<div class="col-md-12 marm-corporate-cover">
				<img src="<?php echo get_option('marmcorporate_image_background'); ?>" alt="Marm Corporate">
				<p><?php echo  get_option_lng('marmcorporate_title'); ?></p>
			</div>
			<!-- Cover Image -->
			<div class="col-md-3 side-panel right">
				<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>
			</div>
			<div class="col-md-9 marm-corporate-desc">
				<br><br><p><?php echo  get_option_lng('marmcorporate_content'); ?></p>

				<div class="marm-corporte-details">
					<div class="marm-corporte-bg">
						<img src="<?php echo theme_folder('themeone'); ?>assets/img/wing-red.png" alt="">
					</div>
					<div class="col-md-10 tour-md-10 icon-listing">
						<h1><?php echo  get_option_lng('marmcorporate_heading'); ?></h1>
						<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/best-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmcorporate_services1'); ?> </li> 
                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/team-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services2'); ?></li>
                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/hotels-icn.png" width="25px" style=" vertical-align: sub;" lt=""> <?php echo  get_option_lng('marmcorporate_services3') ;?></li>
                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/travel-manag-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services4') ;?></li>
                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/meetings-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services5'); ?></li>
                        <li><img src="<?php echo theme_folder('themeone'); ?>assets/img/visa-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmcorporate_services6') ;?></li>
					</div>
				</div>
				<div class="requirement-form">
					<h1><?php echo  get_option_lng('formheading'); ?></h1>
					   <form id="marmcorporate_form" role="form" class="form-horizontal">
                       <input type="hidden" name="subject" value="Mark Corporate" >
					    <label class="control-label col-sm-4" for="company-name"><?php echo  get_option_lng('formfield_1'); ?>:</label>
					    <input type="text" class="form-control" id="company-name"  name="company-name" placeholder="<?php echo  get_option_lng('formfield_1');?>" required>
					    <label class="control-label col-sm-4" for="contact-name"><?php echo  get_option_lng('formfield_2');?>:</label>
					    <input type="text" class="form-control" id="contact-name" name="contact-name" placeholder="<?php echo  get_option_lng('formfield_2');?>" required>
					    <label class="control-label col-sm-4" for="designation"><?php echo  get_option_lng('formfield_3');?>:</label>
					    <input type="text" class="form-control" id="designation" name="designation" placeholder="<?php echo  get_option_lng('formfield_3');?>" required>
					    <label class="control-label col-sm-4" for="address"><?php echo  get_option_lng('formfield_4');?>:</label>
					    <input type="text" class="form-control" id="address" name="address" placeholder="<?php echo  get_option_lng('formfield_4');?>" required>
					    <label class="control-label col-sm-4" for="query"><?php echo  get_option_lng('formfield_5');?>:</label>
					    <textarea id="query" name="query" placeholder="<?php echo  get_option_lng('formfield_5');?>" required></textarea>
						
					    <div style="margin: 0 auto;width: 100%;text-align: center;">
                         <div class="alert alert-success" style="display:none;"  id="submit_marmcorporate">
						 <?= lang('success_email_msg');?>
  </div>
					    	<input type="submit" name="submit"  value="<?php echo  get_option_lng('submitbutton_title');?>">
                             <i id="submit_marmcorporatespin" class="fa fa-spinner fa-spin" style="display:none;font-size:24px;" ></i>
						</div>

					</form>
				</div>
			</div>