	<!-- Cover Image -->

			<div class="col-md-12 no-pad pad-left turkey-cover slider-eff">

<img src="<?php echo get_option('marmflight_image_background'); ?>" alt="Marm Mice">

<div class="caption-inside"><?php echo  get_option_lng('marmflight_title'); ?><br/> <span style="letter-spacing: 3px;color:#bf222e;"><?php echo  get_option_lng('marmflight_subtitle'); ?>  </span></div>


</div>

<!-- Cover Image -->

<div class="col-md-12 no-marg no-pad turkey-bg-n">

<div class="col-md-3 side-panel right turkey-margin caption-nav nav-right-border">

<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>

</div>

<div class="col-md-9 marm-mice-desc">

<br><br><p><?php echo get_option_lng('marmflight_content'); ?></p>



<!--div class="marm-corporte-details">

	<div class="marm-corporte-bg">

		<img src="<?php echo theme_folder('themeone'); ?>assets/img/wing-red.png" alt="">

	</div>

	<div class="col-md-10 tour-md-10 icon-listing">

		<h1><?php echo  get_option_lng('formheading'); ?></h1>

			<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/arrangements-icn.png" width="25px" style=" vertical-align: sub;" alt=""> <?php echo  get_option_lng('marmmice_services1'); ?></li> 

			<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/booking-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmmice_services2'); ?></li>

			<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/conference-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmmice_services3'); ?></li>

			<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/sightseeing-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmmice_services4'); ?></li>

			<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/gala-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmmice_services5'); ?></li>

			<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/activities-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmmice_services6'); ?></li>

			<li><img src="<?php echo theme_folder('themeone'); ?>assets/img/prepost-icn.png" width="25px" style=" vertical-align: sub;" alt="">  <?php echo  get_option_lng('marmmice_services7'); ?></li>

	</div>

</div-->

<div class="requirement-form">

	<h1><?php echo  get_option_lng('marmflightheading_content'); ?></h1>

	<form id="flight_form" role="form" class="form-horizontal">

	                    <label class="control-label col-sm-4" for="name">Name:</label>

						<input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required> 
                        <label class="control-label col-sm-4" for="name">Email:</label>

						<input type="text" class="form-control" id="email" name="email" placeholder="Email" required> 
                        <label class="control-label col-sm-4" for="name">Phone:</label>

						<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone " required> 

		<label class="control-label col-sm-4" for="from">From:</label>

		<input style="width:50%;" type="date" class="form-control" id="from" name="from" placeholder="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d'); ?>" required> 
		
		<label class="control-label col-sm-4" for="to">To:</label>

		<input style="width:50%;" type="date" class="form-control" id="to" name="to" placeholder="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d'); ?>" required> 
		
		<label class="control-label col-sm-4" for="adult">Adult (12 age):</label>

		<input type="text" class="form-control" id="adult" name="adult" placeholder="Adult (12 age)" required> 
		

		<label class="control-label col-sm-4" for="chindren">Chindren (2-12 age):</label>

		<input type="text" class="form-control" id="chindren" name="chindren" placeholder="Chindren (2-12 age)" required> 

		<label class="control-label col-sm-4" for="infant">Infant (0-2 age):</label>

		<input type="text" class="form-control" id="infant" name="infant" placeholder="Infant (0-2 age)" required> 

		<div style="margin: 0 auto;width: 100%;text-align: center;">

		 <div class="alert alert-success" style="display:none;"  id="flight_success">

		 <?= lang('success_form_msg');?>

</div>

			<input type="submit" name="submit"  value="<?php echo  get_option_lng('submitbutton_title');?>">

			 <i id="flight_spin" class="fa fa-spinner fa-spin" style="display:none;font-size:24px;" ></i>

		</div>
<?php /*?> <input type="date" id="arrive" class="floatLabel" name="arrive" value="<?php echo date('Y-m-d'); ?>">
<input type="date" id="depart" class="floatLabel" name="depart" value="<?php echo date('Y-m-d'); ?>" /><?php */?>

	</form>

</div>

</div>
</div>