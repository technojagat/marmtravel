<?php
$postcats=get_post_by_cat('turkey');
$sliders=get_slider("turkey_landing");
?>

			<!-- Cover Image -->
            <div class="col-md-12 no-pad pad-left turkey-cover pad-banner">
					<!-- Slider -->
                            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1319px;height:389px;overflow:hidden;visibility:hidden;">
                    <!-- Loading Screen -->
                    		<div data-u="loading" style="position: absolute; top: 0px; left: 0px;"></div>
                    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1319px;height:389px;overflow:hidden;">
                           <?php

                  if(!empty($sliders)){


					  foreach( $sliders as $sl){

				  ?>
              <div>
               <a href="#" class="slider-eff"><img data-u="image" src="<?php echo $sl['background']; ?>" /></a>
            </div>

				    



				    

                    <?php }} ?>
                        
                      
                        
                    </div>
                    
                    <!-- Arrow Navigator -->
                    <span data-u="arrowleft" class="jssora22l" style="top:0px; left:30px;" data-autocenter="2"></span>
                    <span data-u="arrowright" class="jssora22r" style="top:0px; right:30px;" data-autocenter="2"></span>
             
                        </div>
                        
            <!-- End Slider -->  
                    
                    
					<div class="caption-inside">Turkey<br/> <span>the melting pot</span></div>
				</div>
			
			<!-- Cover Image -->
			<div class="col-md-12 turkey-bg">
            <div class="col-md-3 side-panel right turkey-margin">
			<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>
            </div>
			<div class="col-md-9 turkey-desc">
				<p><?php echo  get_option_lng('turkeylanding_content'); ?></p>
			</div>
			</div>
			
            
            
            <div class="col-md-12 tours no-marg no-pad top-marg">
                   <?php if($postcats){

					

					foreach($postcats as $post){ ?>

                <div class="col-md-3 pad-tour no-marg no-pad slider-eff">
                    	<div class="container-box">
                      		<div class="item-box">
                        		<img src="<?php echo post_thumb($post,'large'); ?>" alt="">
                            </div>
                     	</div>
						<div class="placeholder"><p><?php echo substr(post_title($post),0,10);?></p><span><a href="<?php echo base_url().'tourdetails/index/'.$post->slug?>"><?= feast_line('explore')?></a></span></div>
					</div>

                <?php }} ?>
				
					
                    
                   
                
		  
			</div>
