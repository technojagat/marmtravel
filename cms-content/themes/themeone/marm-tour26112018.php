<!-- Cover Image -->			



<?php

$postobj=(object) array_merge((array) $searchpost[0], (array) $postmeta[0]);





?>



<div class="col-md-12 tour-cover">				







  <img src="<?php  echo post_image($searchpost[0]); ?>" alt="Turkey Cover">				







  <p>







    



  <?=post_title($postobj);?> 



    <br/> 







    <span><?=post_body($postobj);?> 

    </span>







  </p>			







</div>			







<!-- Cover Image -->			







<div class="col-md-3 side-panel right">				







  <?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>			







</div>			







<?php if($totalactivity>0){?>			







<div class="col-md-9 tour-morocco-table">				







  <h1><?= lang('itinerary');?>







  </h1>				







  <table class="table table-striped" style="font-size:13px;">				  







    <thead>				    







      <tr>				      







        <th style="color:#bf222e;font-size:17px" scope="col"><?= lang('date');?>







        </th>				      







        <th style="color:#bf222e;font-size:17px" scope="col"><?= lang('activity');?>







        </th>				      







        <th style="color:#bf222e;font-size:17px" scope="col"><?= lang('hotel');?>







        </th>				    







      </tr>				  







    </thead>				  







    <tbody>







      <?php foreach($postactivity as $d){?>				    







      <tr>				      







        <th scope="row">







         



		<?=changeLanguage($d,'day',$d->day_eng);?> 



        </th>				      







        <td>







         



          <?=changeLanguage($d,'activity',$d->activity_eng);?> 







        </td>				      







        <td>







          



           <?=changeLanguage($d,'hotel',$d->hotel_eng);?>







        </td>				    







      </tr>                    







      <?php }?>				    				  







    </tbody>				







  </table>			







</div>							







<div class="col-md-9 tour-desc">				







  <?php foreach($postactivity as $d){?>				







  <h2>







 



<?=changeLanguage($d,'day',$d->day_eng);?> 



<?=changeLanguage($d,'activity',$d->activity_eng);?> 



  







  </h2>				







  <p> 



<?=changeLanguage($d,'details',$d->details_eng);?> 



    







  </p>					







  <?php } ?>				







  <!--img src="<?php echo theme_folder('themeone'); ?>assets/img/tour-content.JPG" width="60%" alt="">					<h2>Day 2  Fes </h2>				<p>Today you will take a step back in time to the Middle Ages when you will visit Fes El Bali, the largest living medieval medina-city and the cultural heart of Morocco. You will explore some of the 9000 narrow lanes, alleys and souks that make-up the labyrinth of the city's old quarter, originally founded in the 8th century AD by Moulay Idriss I. The medieval Medina is a UNESCO World Heritage Site. </p-->					







</div>		







<?php }?>		







</div>		







<div class="tour-price">		







  <div class="tour-price-bg">			







    <img src="<?php echo theme_folder('themeone'); ?>assets/img/wing.png" alt="">		







  </div>			







  <div class="col-md-10 tour-md-10">				







    <h1><?= lang('price_heading');?>







    </h1>				







    <?=changeLanguage($postmeta[0],'price',$searchpost[0]->price);?> 			







  </div>		







</div>		







<div class="tour-flight">			







  <div class="col-md-10 tour-md-10">				







    <h1><?= lang('flight_heading');?>







    </h1>				







     <?=changeLanguage($postmeta[0],'flight',$searchpost[0]->flight);?>			







  </div>		







</div>		







<div class="container">			







  <div class="col-md-2 tour-download border-right">				







    <p><?= lang('tour_download');?>







    </p>				





<?php $filepath=base_url().'cms-content/uploads/'.changeLanguage($searchpost[0],'file',$searchpost[0]->file_english);?>

    <a href="<?=$filepath?>" download>







      <img src="<?php echo theme_folder('themeone'); ?>assets/img/download-pdf-icon.png" alt="">







    </a>			







  </div>		







    <div class="col-md-2 tour-download border-right">				







    <p><?= lang('send_your_friend');?>







    </p>				







   <button type="button" class="btn btn-red btn-lg" data-toggle="modal" data-target="#referFriend"><?= lang('send_to_friend');?></button>		







  </div>		







  <div class="col-md-4 itinerary-download">				







    <p> <?= lang('tour_subscribe_heading');?>







    </p>				







    <input type="text" id="submit_subscribe_tour" placeholder="<?= lang('subscribe_placeholder');?>" required>				







    <input type="submit" onClick="submit_subscribe_tour()" value="<?= lang('subscribe_button')?>"><i id="submit_subscribe_tourspin" class="fa fa-spinner fa-spin" style="display:none;" ></i>		



 <div class="alert alert-success" style="display:none;"  id="submit_subscribe_tour2">



    <?= lang('success_email_msg');?>



  </div>



  </div>	







  	







</div>







<div class="modal fade" id="referFriend" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">







					<div class="modal-dialog" role="document">







					<div class="modal-content">







					<div class="modal-header">







	          <button type="button" class="close grey" data-dismiss="modal">&times;</button>







	          <h4 class="modal-title"><?= lang('send_to_friend');?></h4>







	        </div>







				<div class="modal-body">







					<div class="form-area">  







        <form id="sendfriend_form" role="form">







        <br style="clear:both">







    				<div class="form-group" style="width:auto">







						<input type="text" class="form-control" id="name" name="name" placeholder="<?= lang('your_name');?>" required>







					</div>







					<div class="form-group" style="width:auto">







						<input type="email" class="form-control" id="email" name="email" placeholder="<?= lang('your_email');?>" required>







					</div>







					<div class="form-group" style="width:auto">







						<input type="text" class="form-control" id="recipient" name="recipient" placeholder="<?= lang('recipient_name');?>" required>







					</div>







					<div class="form-group" style="width:auto">







						<input type="email" class="form-control" id="to" name="to" placeholder="<?= lang('recipient_email');?>" required>







					</div>







					<div class="form-group" style="width:auto">







						<input type="text" class="form-control" id="subject" name="subject" placeholder="<?= lang('subject');?>" required>







					</div>



<input type="hidden"   name="refurl"  value="<?= $_SERVER['HTTP_REFERER'] ?>">



                    <div class="form-group" style="width:auto;height:auto;">







                    <textarea class="form-control" type="textarea" name="textarea" id="message" placeholder="<?= lang('message');?>" rows="7" required></textarea>                   







                    </div>







            <div class="alert alert-success" style="display:none;"  id="successMessage2">



    <?= lang('success_email_msg');?>



  </div>







        <button type="button" id="submit" name="submit" onClick="submit_sendafriend()" class="btn btn-red pull-right"><?= lang('send_button');?> 



        <i id="successMessage2spin" class="fa fa-spinner fa-spin" style="display:none;" ></i></button>







        </form>







    </div>







				</div>







				<!--div class="modal-footer">







					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>







				</div-->







				</div>







				</div>







			</div>







