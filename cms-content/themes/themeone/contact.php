	<!-- Cover Image -->

			<div class="col-md-12 no-pad pad-left turkey-cover slider-eff">

				<img src="<?php echo get_option('marmcontact_image_background'); ?>" alt="Turkey Cover">

				<div class="caption-inside"><?php echo  get_option_lng('marmcontact_title'); ?><br/> <span style="letter-spacing: 3px;color:#bf222e;"><?php echo  get_option_lng('marmcontact_subtitle'); ?>  </span></div>

			</div>

			<!-- Cover Image -->

			<div class="col-md-12 no-marg no-pad turkey-bg-n">

			<div class="col-md-3 side-panel right turkey-margin caption-nav nav-right-border">

	<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>

			</div>

			<div class="col-md-9" style="padding:0;">

				
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d24114.896363579253!2d29.3017928!3d40.9297318!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cada1b5dae3f4f%3A0x2873a25a1f8a7cb!2sAirPort+Plaza!5e0!3m2!1sen!2sin!4v1543633201083" width="100%" height="340" frameborder="0" style="border:0" allowfullscreen></iframe>

			</div>

		<div class="container  height-contact-details">

			<div class="col-md-4 contact-details contact-details-border-right">

				<h1><?php echo get_option_lng('marm_heading1'); ?></h1>



				<p><?php echo get_option('marm_heading1_content'); ?> </p>

			</div>

			<div class="col-md-3 help-desk">

				<h1><?php echo get_option_lng('marm_heading2'); ?></h1>

				<p><?php echo get_option('marm_heading2_content'); ?> </p>

			</div>

			<div class="col-md-3 help-desk" style="border-right:0;">

				<h1><?php echo get_option_lng('marm_heading3'); ?> </h1>

				<p><?php echo get_option('marm_heading3_content'); ?></p>

			</div>

		</div>
        
        </div>