


			<!-- Cover Image -->
			<div class="col-md-12 marm-mice-cover">
				<img src="<?php echo get_option('network_image_background'); ?>" alt="Network Partner">
				<p><?= get_option_lng('marm_network_title');?> <br/> <span style="letter-spacing: 3px;"> Marm</span></p>
			</div>
			<!-- Cover Image -->
			<div class="col-md-3 side-panel right">
		<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>
			</div>
			<div class="col-md-9 marm-mice-desc">
				<br><br><p><?= get_option_lng('network_content')?></p>

				<?= get_option('network_logo')?>
			</div>
	