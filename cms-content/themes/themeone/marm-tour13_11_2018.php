
			<!-- Cover Image -->
			<div class="col-md-12 tour-cover">
				<img src="<?php echo theme_folder('themeone'); ?>assets/img/tour-cover.JPG" alt="Turkey Cover">
				<p>Morocco <br/> <span>mystically yours</span></p>
			</div>
			<!-- Cover Image -->
			<div class="col-md-3 side-panel right">
				<?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>
			</div>
			<div class="col-md-9 tour-morocco-table">
				<h1>Itinerary Summery</h1>
				<table class="table table-striped" style="font-size:13px;">
				  <thead>
				    <tr>
				      <th style="color:#bf222e;font-size:17px" scope="col">Date</th>
				      <th style="color:#bf222e;font-size:17px" scope="col">Activity</th>
				      <th style="color:#bf222e;font-size:17px" scope="col">Hotel</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">Day 1</th>
				      <td>Istanbul-Casablanca-Fes</td>
				      <td>Riad Myra</td>
				    </tr>
				    <tr>
				      <th scope="row">Day 2</th>
				      <td>Fes</td>
				      <td>Riad Myra</td>
				    </tr>
				    <tr>
				      <th scope="row">Day 3</th>
				      <td>Fes-Marrakech</td>
				      <td>Les Jardins de la Medina</td>
				    </tr>
				    <tr>
				      <th scope="row">Day 4</th>
				      <td>Marrakech</td>
				      <td>Les Jardins de la Medina</td>
				    </tr>
				    <tr>
				      <th scope="row">Day 5</th>
				      <td>Marrakech-Essaouira- Marrakech</td>
				      <td>Les Jardins de la Medina</td>
				    </tr>
				    <tr>
				      <th scope="row">Day 6</th>
				      <td>Marrakech-Casablanca</td>
				      <td>Hyatt Regency Casablanca</td>
				    </tr>
				    <tr>
				      <th scope="row">Day 7</th>
				      <td>Casablanca-Istanbul</td>
				      <td></td>
				    </tr>
				  </tbody>
				</table>
			</div>
			<div class="col-md-9 tour-desc">
				<h2>Day 1 Istanbul-Casablanca-Fes </h2>
				<p>Istanbul-Casablanca-Fes  Flight from Istanbul to Casablanca at 10.50 by Turkish Airlines. Arrive to Casablanca at 13.00.  <br /><br />
 
				You will be transferred to the cultural heart of Morocco, magical Fes. Founded in the 8th century, it is home to several famed historical monuments, among them Karaouine, the world's first university. The sprawling medina of this UNESCO Cultural Heritage site will take you a step back in time to the middle Ages. <br /><br />
				 
				Overnight stay at hotel. </p>	

				<img src="<?php echo theme_folder('themeone'); ?>assets/img/tour-content.JPG" width="60%" alt="">	

				<h2>Day 2  Fes </h2>
				<p>Today you will take a step back in time to the Middle Ages when you will visit Fes El Bali, the largest living medieval medina-city and the cultural heart of Morocco. You will explore some of the 9000 narrow lanes, alleys and souks that make-up the labyrinth of the city's old quarter, originally founded in the 8th century AD by Moulay Idriss I. The medieval Medina is a UNESCO World Heritage Site. </p>		
			</div>
		</div>
		<div class="tour-price">
		<div class="tour-price-bg">
			<img src="<?php echo theme_folder('themeone'); ?>assets/img/wing.png" alt="">
		</div>
			<div class="col-md-10 tour-md-10">
				<h1>Price Information</h1>
				<p>
					<b>Cost Includes</b>
					<li>Hotel accommodation (Hotels could be replaced by similar hotel in same category),</li> 
					<li>Lunches and dinners,</li>
					<li>English speaking national guide, </li>
					<li>All Transfer & transportation, </li>
					<li>Entrance fees, </li>
					<li>½ litter mineral water per day,</li> 
					<li>All taxes. </li>
				</p>
				<p>	 
					<b>Cost Does Not Include</b>  
					<li>International flight tickets between Istanbul-Casablanca-Istanbul,</li> 
					<li>Tip for Guide and Drivers,</li> 
					<li>Travel Insurance, </li>
					<li>Personal Expenses.</li> 
				</p>
			</div>
		</div>
		<div class="tour-flight">
			<div class="col-md-10 tour-md-10">
				<h1>Suggested Flight Details</h1>
				<p> Turkish Airlines / Istanbul - Casablanca / TK 617 / 10.50 departure - 13.00 arrival</p>
				<p> Turkish Airlines / Casablanca - Istanbu" placl / TK 618 / 14.40 departure - 22.10 arrival</p>
			</div>
		</div>
		<div class="container">
			<div class="col-md-2 tour-download border-right">
				<p>Download Itinerary</p>
				<a href=""><img src="<?php echo theme_folder('themeone'); ?>assets/img/download-pdf-icon.png" alt=""></a>
			</div>
			<div class="col-md-6 itinerary-download">
				<p>Interested in this tour? <br />
				Contact with us.</p>
				<input type="text" placeholder="Enter your email address">
				<input type="submit" value="subscribe">
			</div>
		</div>