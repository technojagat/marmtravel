<!-- Cover Image -->			
<div class="col-md-12 tour-cover">				
  <img src="<?php echo theme_folder('themeone'); ?>assets/img/tour-cover.JPG" alt="Turkey Cover">				
  <p>
    <?=$searchpost[0]->title?> 
    <br/> 
    <span>mystically yours
    </span>
  </p>			
</div>			
<!-- Cover Image -->			
<div class="col-md-3 side-panel right">				
  <?php $this->load->view($this->pref->active_theme.'/layouts/includes/sidebar'); ?>			
</div>			
<?php if($totalactivity>0){?>			
<div class="col-md-9 tour-morocco-table">				
  <h1>Itinerary Summery
  </h1>				
  <table class="table table-striped" style="font-size:13px;">				  
    <thead>				    
      <tr>				      
        <th style="color:#bf222e;font-size:17px" scope="col">Date
        </th>				      
        <th style="color:#bf222e;font-size:17px" scope="col">Activity
        </th>				      
        <th style="color:#bf222e;font-size:17px" scope="col">Hotel
        </th>				    
      </tr>				  
    </thead>				  
    <tbody>
      <?php foreach($postactivity as $d){?>				    
      <tr>				      
        <th scope="row">
          <?=$d->day_eng?>
        </th>				      
        <td>
          <?=$d->activity_eng?>
        </td>				      
        <td>
          <?=$d->hotel_eng?>
        </td>				    
      </tr>                    
      <?php }?>				    				  
    </tbody>				
  </table>			
</div>							
<div class="col-md-9 tour-desc">				
  <?php foreach($postactivity as $d){?>				
  <h2>
    <?=$d->day_eng?> 
    <?=$d->activity_eng?> 
  </h2>				
  <p> 
    <?=$d->details_eng?>
  </p>					
  <?php } ?>				
  <!--img src="<?php echo theme_folder('themeone'); ?>assets/img/tour-content.JPG" width="60%" alt="">					<h2>Day 2  Fes </h2>				<p>Today you will take a step back in time to the Middle Ages when you will visit Fes El Bali, the largest living medieval medina-city and the cultural heart of Morocco. You will explore some of the 9000 narrow lanes, alleys and souks that make-up the labyrinth of the city's old quarter, originally founded in the 8th century AD by Moulay Idriss I. The medieval Medina is a UNESCO World Heritage Site. </p-->					
</div>		
<?php }?>		
</div>		
<div class="tour-price">		
  <div class="tour-price-bg">			
    <img src="<?php echo theme_folder('themeone'); ?>assets/img/wing.png" alt="">		
  </div>			
  <div class="col-md-10 tour-md-10">				
    <h1>Price Information
    </h1>				
    <p>					
      <b>Cost Includes
      </b>					
      <li>Hotel accommodation (Hotels could be replaced by similar hotel in same category),
      </li> 					
      <li>Lunches and dinners,
      </li>					
      <li>English speaking national guide, 
      </li>					
      <li>All Transfer & transportation, 
      </li>					
      <li>Entrance fees, 
      </li>					
      <li>½ litter mineral water per day,
      </li> 					
      <li>All taxes. 
      </li>				
    </p>				
    <p>	 					
      <b>Cost Does Not Include
      </b>  					
      <li>International flight tickets between Istanbul-Casablanca-Istanbul,
      </li> 					
      <li>Tip for Guide and Drivers,
      </li> 					
      <li>Travel Insurance, 
      </li>					
      <li>Personal Expenses.
      </li> 				
    </p>			
  </div>		
</div>		
<div class="tour-flight">			
  <div class="col-md-10 tour-md-10">				
    <h1>Suggested Flight Details
    </h1>				
    <p> Turkish Airlines / Istanbul - Casablanca / TK 617 / 10.50 departure - 13.00 arrival
    </p>				
    <p> Turkish Airlines / Casablanca - Istanbu" placl / TK 618 / 14.40 departure - 22.10 arrival
    </p>			
  </div>		
</div>		
<div class="container">			
  <div class="col-md-2 tour-download border-right">				
    <p>Download Itinerary
    </p>				
    <a href="">
      <img src="<?php echo theme_folder('themeone'); ?>assets/img/download-pdf-icon.png" alt="">
    </a>			
  </div>		
    <div class="col-md-2 tour-download border-right">				
    <p>Send to your  Friend
    </p>				
   <button type="button" class="btn btn-red btn-lg" data-toggle="modal" data-target="#referFriend">Send to Friend</button>		
  </div>		
  <div class="col-md-4 itinerary-download">				
    <p>Interested in this tour? 
      <br />				Contact with us.
    </p>				
    <input type="text" placeholder="Enter your email address">				
    <input type="submit" value="subscribe">			
  </div>	
  	
</div>
<div class="modal fade" id="referFriend" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
					<div class="modal-content">
					<div class="modal-header">
	          <button type="button" class="close grey" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Send to a Friend</h4>
	        </div>
				<div class="modal-body">
					<div class="form-area">  
        <form role="form">
        <br style="clear:both">
    				<div class="form-group" style="width:auto">
						<input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required>
					</div>
					<div class="form-group" style="width:auto">
						<input type="email" class="form-control" id="email" name="email" placeholder="Your Email" required>
					</div>
					<div class="form-group" style="width:auto">
						<input type="text" class="form-control" id="recipient" name="recipient" placeholder="Recipient's Name" required>
					</div>
					<div class="form-group" style="width:auto">
						<input type="email" class="form-control" id="to" name="to" placeholder="Recipient's Email" required>
					</div>
					<div class="form-group" style="width:auto">
						<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
					</div>
                    <div class="form-group" style="width:auto;height:auto;">
                    <textarea class="form-control" type="textarea" id="message" placeholder="Message" rows="7" required></textarea>                   
                    </div>
            
        <button type="button" id="submit" name="submit" class="btn btn-red pull-right">Send</button>
        </form>
    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
				</div>
				</div>
			</div>
