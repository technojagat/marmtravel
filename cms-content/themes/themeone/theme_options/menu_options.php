<?php if(is_rtl() == TRUE) {?>
<style type="text/css">
.question-icon i { border-right: 1px solid #555; border-left: none}
</style>
<?php }
  $layouts = array(
              '2cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/2cols-sidebar.png',
              '3cols' =>  THEME_FOLDER.'assets/images/blog/3cols.png',
              '3cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/3cols-sidebar.png',
              '4cols' =>  THEME_FOLDER.'assets/images/blog/4cols.png',
              'classic-right' =>  THEME_FOLDER.'assets/images/blog/classic-right.png',
              'classic-left' =>  THEME_FOLDER.'assets/images/blog/classic-left.png'
          );

  $portfolio = array(
              '2cols' =>  '2 Cols',
              '3cols' =>  '3 Cols',
              '4cols' =>  '4 Cols'
          );

?>

<div class="tabbable nav-tabs-custom tabs-<?=is_rtl() == TRUE ? 'right' : 'left'?>" role="tabpanel">
  <ul class="nav nav-tabs">
    <!--   <li class="active"><a data-toggle="tab" href="#general">General options</a></li>-->
    <li class="active"><a data-toggle="tab" href="#menuoptions">Menu Options</a></li>
    <li><a data-toggle="tab" href="#hoteloptions">Hotel Options</a></li>
    <li><a data-toggle="tab" href="#flightoptions">Flight Options</a></li>
  </ul>

  <div class="tab-content full-content">
    <div id="menuoptions" class="tab-pane fade in active">
      <h3>Menu Options</h3>
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 1', 'marm_menu_1', get_option('marm_menu_1'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 1 ', 'marm_menu_1_turkish', get_option('marm_menu_1_turkish'), 'Bir menü ismi girin.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 2', 'marm_menu_2', get_option('marm_menu_2'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 2', 'marm_menu_2_turkish', get_option('marm_menu_2_turkish'), 'Bir menü ismi girin.')?>
      </div>

          <!-- Submenus starts-->
          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu 1', 'marm_submenu_1', get_option('marm_submenu_1'), 'Enter a submenu name.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu Turkish 1', 'marm_submenu_1_turkish', get_option('marm_submenu_1_turkish'), 'Bir alt menü adı girin.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu 2', 'marm_submenu_2', get_option('marm_submenu_2'), 'Enter a menu name.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu Turkish 2', 'marm_submenu_2_turkish', get_option('marm_submenu_2_turkish'), 'Bir alt menü adı girin.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu 3', 'marm_submenu_3', get_option('marm_submenu_3'), 'Enter a menu name.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu Turkish 3', 'marm_submenu_3_turkish', get_option('marm_submenu_3_turkish'), 'Bir alt menü adı girin.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu 4', 'marm_submenu_4', get_option('marm_submenu_4'), 'Enter a menu name.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu Turkish 4', 'marm_submenu_4_turkish', get_option('marm_submenu_4_turkish'), 'Bir alt menü adı girin.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu 5', 'marm_submenu_5', get_option('marm_submenu_5'), 'Enter a menu name.')?>
          </div>

          <div class="col-xs-12">
            <?=admin_input_text('Marm Submenu Turkish 5', 'marm_submenu_5_turkish', get_option('marm_submenu_5_turkish'), 'Bir alt menü adı girin.')?>
          </div>
          <!-- Sub Menus ends-->

      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 3', 'marm_menu_3', get_option('marm_menu_3'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 3', 'marm_menu_3_turkish', get_option('marm_menu_3_turkish'), 'Bir menü ismi girin.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 4', 'marm_menu_4', get_option('marm_menu_4'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 4', 'marm_menu_4_turkish', get_option('marm_menu_4_turkish'), 'Bir menü ismi girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 5', 'marm_menu_5', get_option('marm_menu_5'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 5', 'marm_menu_5_turkish', get_option('marm_menu_5_turkish'), 'Bir menü ismi girin.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 6', 'marm_menu_6', get_option('marm_menu_6'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 6', 'marm_menu_6_turkish', get_option('marm_menu_6_turkish'), 'Bir menü ismi girin.')?>
      </div>   

       <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 7', 'marm_menu_7', get_option('marm_menu_7'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 7', 'marm_menu_7_turkish', get_option('marm_menu_7_turkish'), 'Bir menü ismi girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 8', 'marm_menu_8', get_option('marm_menu_8'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 8', 'marm_menu_8_turkish', get_option('marm_menu_8_turkish'), 'Bir menü ismi girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu 9', 'marm_menu_9', get_option('marm_menu_9'), 'Enter a menu name.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Marm Menu Turkish 9', 'marm_menu_9_turkish', get_option('marm_menu_9_turkish'), 'Bir menü ismi girin.')?>
      </div>

    </div>

    <div id="hoteloptions" class="tab-pane fade">

      <h3>Hotel Options</h3>
      <div class="col-xs-12">
        <?=admin_input_text('Title', 'marmhotel_title', get_option('marmhotel_title'), 'Set a title.')?>
      </div>
      <div class="col-xs-12">
        <?=admin_input_text('Title Turkish', 'marmhotel_title_turkish', get_option('marmhotel_title_turkish'), 'Bir başlık ayarla.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Subtitle', 'marmhotel_subtitle', get_option('marmhotel_subtitle'), 'Set a subtitle.')?>
      </div>
      <div class="col-xs-12">
        <?=admin_input_text('Subtitle Turkish', 'marmhotel_subtitle_turkish', get_option('marmhotel_subtitle_turkish'), 'Altyazı ayarla.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_upload('Image background', 'marmhotel_image_background', get_option('marmhotel_image_background'), 'Select background image for static image header.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'marmhotel_content', get_option('marmhotel_content'), 'Enter the content.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Enter Content Turkish', 'marmhotel_content_turkish', get_option('marmhotel_content_turkish'), 'Enter the content.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Heading', 'marmhotelheading_content', get_option('marmhotelheading_content'), 'Enter the form heading.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Heading Turkish', 'marmhotelheading_content_turkish', get_option('marmhotelheading_content_turkish'), 'Enter the form heading.')?>
      </div>

  </div>

  <div id="flightoptions" class="tab-pane fade">

    <h3>Flight Options</h3>
    <div class="col-xs-12">
      <?=admin_input_text('Title', 'marmflight_title', get_option('marmflight_title'), 'Set a title.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Title Turkish', 'marmflight_title_turkish', get_option('marmflight_title_turkish'), 'Bir başlık ayarla.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Subtitle', 'marmflight_subtitle', get_option('marmflight_subtitle'), 'Set a subtitle.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Subtitle Turkish', 'marmflight_subtitle_turkish', get_option('marmflight_subtitle_turkish'), 'Altyazı ayarla.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_upload('Image background', 'marmflight_image_background', get_option('marmflight_image_background'), 'Select background image for static image header.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content', 'marmflight_content', get_option('marmflight_content'), 'Enter the content.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content Turkish', 'marmflight_content_turkish', get_option('marmflight_content_turkish'), 'Enter the content.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Form Heading', 'marmflightheading_content', get_option('marmflightheading_content'), 'Enter the form heading.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Form Heading Turkish', 'marmflightheading_content_turkish', get_option('marmflightheading_content_turkish'), 'Enter the form heading.')?>
    </div>

    </div>
</div>