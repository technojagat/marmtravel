<?php if(is_rtl() == TRUE) {?>
<style type="text/css">
.question-icon i { border-right: 1px solid #555; border-left: none}
</style>
<?php }
  $layouts = array(
              '2cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/2cols-sidebar.png',
              '3cols' =>  THEME_FOLDER.'assets/images/blog/3cols.png',
              '3cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/3cols-sidebar.png',
              '4cols' =>  THEME_FOLDER.'assets/images/blog/4cols.png',
              'classic-right' =>  THEME_FOLDER.'assets/images/blog/classic-right.png',
              'classic-left' =>  THEME_FOLDER.'assets/images/blog/classic-left.png'
          );

  $portfolio = array(
              '2cols' =>  '2 Cols',
              '3cols' =>  '3 Cols',
              '4cols' =>  '4 Cols'
          );

?>

<div class="tabbable nav-tabs-custom tabs-<?=is_rtl() == TRUE ? 'right' : 'left'?>" role="tabpanel">
  <ul class="nav nav-tabs">
 <!--   <li class="active"><a data-toggle="tab" href="#general">General options</a></li>-->
    <li class="active"><a data-toggle="tab" href="#header">Homepage options</a></li>
    <li ><a data-toggle="tab" href="#marmxclusive">Marmxclusive options</a></li>
    <li><a data-toggle="tab" href="#blog">Blog options</a></li>
    <li><a data-toggle="tab" href="#footer">Footer Options</a></li>
  </ul>

  <div class="tab-content full-content">
<?php /*?>    <div id="general" class="tab-pane fade in active">
      <h3>General options</h3>

      <div class="col-xs-12">
        <?=admin_input_spinner('Posts per page', 'posts_per_page', get_option('posts_per_page'), 'Set number of posts per page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_spinner('Search posts per page', 'search_per_page', get_option('search_per_page'), 'Set number of posts on search page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_spinner('Category posts per page', 'category_per_page', get_option('category_per_page'), 'Set number of posts on category page.')?>
      </div>

    </div><?php */?>

    <div id="header" class="tab-pane fade in active">

<?php /*?>      <div class="col-xs-12">
        <?=admin_select('Home page Header style', 'themeone_header_layout', array('Flex slider' => 'slider', 'Text rotate' => 'textrotate', 'Static image' => 'static_image', 'Gradient overlay' => 'gradient'), get_option('themeone_header_layout'), 'Select a home page header style.')?>
      </div>      

      <div class="col-xs-12">
        <?=admin_radio ('Home page Fullscreen header', 'themeone_header_fullscreen', array(1 => 'Fullscreen', 0 => 'Classic'), get_option('themeone_header_fullscreen'), 'Select a home page header layout Fullscreen/Classic.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_select('Home slider', 'home_slider', sliders_list(), get_option('home_slider'), 'Select a slider for home page.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_upload('Textrotate background', 'themeone_textrotate_image', get_option('themeone_textrotate_image'), 'Select background image for Textrotate header.')?>
      </div><?php */?>

      <div class="col-xs-12">
        <?=admin_textarea('Experience  title', 'themeone_textrotate_title', get_option('themeone_textrotate_title'), 'Enter a title for Textrotate header.')?>
      </div>

  <div class="col-xs-12">
        <?=admin_textarea('Experience  title Turkish', 'themeone_textrotate_title_turkish', get_option('themeone_textrotate_title_turkish'), 'Enter a title for Textrotate header.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_textarea('Experience  texts', 'themeone_textrotate', get_option('themeone_textrotate'), 'Enter texts for textslider (separate them by comma ",").')?>
      </div>
      
        <div class="col-xs-12">
        <?=admin_textarea('Experience  texts Turkish', 'themeone_textrotate_turkish', get_option('themeone_textrotate_turkish'), 'Enter texts for textslider (separate them by comma ",").')?>
      </div>
      
<?php /*?>      <div class="col-xs-12">
        <?=admin_upload('Static image background 1', 'themeone_header_image', get_option('themeone_header_image'), 'Select background image for static image header.')?>
      </div>
      <div class="col-xs-12">
        <?=admin_upload('Static image background 2', 'themeone_header_image2', get_option('themeone_header_image2'), 'Select background image for static image header.')?>
      </div><?php */?>
   <div class="col-xs-12">
        <?=admin_input_text('Other Tours Title', 'title_others_tours', get_option('title_others_tours'), 'Enter a title for others tour.')?>
      </div>
      
         <div class="col-xs-12">
        <?=admin_input_text('Other Tours Title Turkish', 'title_others_tours_turkish', get_option('title_others_tours_turkish'), 'Enter a title for others tour.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Static image title One', 'themeone_header_image_title', get_option('themeone_header_image_title'), 'Enter a title for static image header.')?>
      </div>
  
       <div class="col-xs-12">
        <?=admin_input_text('Static image title One Turkish', 'themeone_header_image_title_turkish', get_option('themeone_header_image_title_turkish'), 'Enter a title for static image header.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_textarea('Static image text One', 'themeone_header_image_text', get_option('themeone_header_image_text'), 'Enter a welcome text for static image header.')?>
      </div>

    <div class="col-xs-12">
        <?=admin_textarea('Static image text One Turkish', 'themeone_header_image_text_turkish', get_option('themeone_header_image_text_turkish'), 'Enter a welcome text for static image header.')?>
      </div>
   <div class="col-xs-12">
        <?=admin_input_text('Static image title Two', 'themeone_header_image_title_2', get_option('themeone_header_image_title_2'), 'Enter a title for static image header.')?>
      </div>
  
    <div class="col-xs-12">
        <?=admin_input_text('Static image title Two Turkish', 'themeone_header_image_title_2_turkish', get_option('themeone_header_image_title_2_turkish'), 'Enter a title for static image header.')?>
      </div>
  
  
      <div class="col-xs-12">
        <?=admin_textarea('Static image text Two', 'themeone_header_image_text_2', get_option('themeone_header_image_text_2'), 'Enter a welcome text for static image header.')?>
      </div>
     
        <div class="col-xs-12">
        <?=admin_textarea('Static image text Two Turkish', 'themeone_header_image_text_2_turkish', get_option('themeone_header_image_text_2_turkish'), 'Enter a welcome text for static image header.')?>
      </div>
      
       
         <div class="col-xs-12">
        <?=admin_input_text('Static image title Three', 'themeone_header_image_title_3', get_option('themeone_header_image_title_3'), 'Enter a title for static image header.')?>
      </div>
      
            <div class="col-xs-12">
        <?=admin_input_text('Static image title Three Turkish', 'themeone_header_image_title_3_turkish', get_option('themeone_header_image_title_3_turkish'), 'Enter a title for static image header.')?>
      </div>
  
      <div class="col-xs-12">
        <?=admin_textarea('Static image text Three', 'themeone_header_image_text_3', get_option('themeone_header_image_text_3'), 'Enter a welcome text for static image header.')?>
      </div>
      
      
         <div class="col-xs-12">
        <?=admin_textarea('Static image text Three Turkish', 'themeone_header_image_text_3_turkish', get_option('themeone_header_image_text_3_turkish'), 'Enter a welcome text for static image header.')?>
      </div>
      
 <div class="col-xs-12">
        <?=admin_input_text('Subscribe  Title', 'subscribe_title', get_option('subscribe_title'), 'Enter a title.')?>
 </div>
  <div class="col-xs-12">
        <?=admin_input_text('Subscribe  Title Turkish', 'subscribe_title_turkish', get_option('subscribe_title_turkish'), 'Enter a title.')?>
 </div>

 <div class="col-xs-12">
        <?=admin_input_text('Subscribe  Placeholder', 'subscribe_Placeholder', get_option('subscribe_Placeholder'), 'Enter a title.')?>
 </div>
 
  <div class="col-xs-12">
        <?=admin_input_text('Subscribe  Placeholder Turkish', 'subscribe_Placeholder_turkish', get_option('subscribe_Placeholder_turkish'), 'Enter a title.')?>
 </div>
 
 <div class="col-xs-12">
        <?=admin_input_text('Subscribe  Button', 'subscribe_button', get_option('subscribe_button'), 'Enter a title.')?>
 </div>
 <div class="col-xs-12">
        <?=admin_input_text('Subscribe  Button Turkish', 'subscribe_button_turkish', get_option('subscribe_button_turkish'), 'Enter a title.')?>
 </div> 
    </div>

    <div id="blog" class="tab-pane fade">
      <h3>Blog options</h3>

      <div class="col-xs-12">
        <?=admin_radio_image ('Blog layout', 'themeone_blog_layout', $layouts, get_option('themeone_blog_layout'), 'Select a layout for blog page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio_image ('Category layout', 'themeone_category_layout', $layouts, get_option('themeone_category_layout'), 'Select a layout for category page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio_image ('Search page layout', 'themeone_search_layout', $layouts, get_option('themeone_search_layout'), 'Select a layout for search page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio ('portfolio page layout', 'themeone_portfolio_layout', $portfolio, get_option('themeone_portfolio_layout'), 'Select a layout for portfolio page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio ('portfolio Boxed', 'themeone_portfolio_boxed', NULL, get_option('themeone_portfolio_boxed'), 'Select portfolio layout Boxed/Full width.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio ('portfolio Gutter', 'themeone_portfolio_gutter', NULL, get_option('themeone_portfolio_gutter'), 'Select portfolio gutter layout.')?>
      </div>            

    </div>

    <div id="marmxclusive" class="tab-pane fade">
      <h3>Marmxclusive Options</h3>
      <div class="col-xs-12">
       
      </div>

   <div class="col-xs-12">
      <?=admin_input_text('Marmxclusive Title', 'marmxclusive_title', get_option('marmxclusive_title'), 'Set a title.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Marmxclusive Title Turkish', 'marmxclusive_title_turkish', get_option('marmxclusive_title_turkish'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_upload('Marmxclusive image background', 'marmxclusive_image_background', get_option('marmxclusive_image_background'), 'Select background image for static image header.')?>
      </div>

 <div class="col-xs-12">
      <?=admin_textarea('Footer Content Turkish', 'footer_main_content_turkish', get_option('footer_main_content_turkish'), 'Set a content fot footer widget.')?>
      </div>
       <div class="col-xs-12">
      <?=admin_textarea('Footer Address', 'footer_address_content', get_option('footer_address_content'), 'Set a content fot footer address.')?>
      </div>
      
      <div class="col-xs-12">
      <?=admin_input_text('Footer Phone', 'footer_phone', get_option('footer_phone'), 'Set a footer phone.')?>
      </div>

 <div class="col-xs-12">
      <?=admin_input_text('Footer Email', 'footer_email', get_option('footer_email'), 'Set a footer email.')?>
      </div>
 

    </div>


    <div id="footer" class="tab-pane fade">
      <h3>Footer Options</h3>
      <div class="col-xs-12">
       
      </div>

   <div class="col-xs-12">
      <?=admin_textarea('Footer Content', 'footer_main_content', get_option('footer_main_content'), 'Set a content fot footer widget.')?>
      </div>
      
 <div class="col-xs-12">
      <?=admin_textarea('Footer Content Turkish', 'footer_main_content_turkish', get_option('footer_main_content_turkish'), 'Set a content fot footer widget.')?>
      </div>
       <div class="col-xs-12">
      <?=admin_textarea('Footer Address', 'footer_address_content', get_option('footer_address_content'), 'Set a content fot footer address.')?>
      </div>
      
      <div class="col-xs-12">
      <?=admin_input_text('Footer Phone', 'footer_phone', get_option('footer_phone'), 'Set a footer phone.')?>
      </div>

 <div class="col-xs-12">
      <?=admin_input_text('Footer Email', 'footer_email', get_option('footer_email'), 'Set a footer email.')?>
      </div>
 

    </div>

  </div>
</div>