<?php if(is_rtl() == TRUE) {?>
<style type="text/css">
.question-icon i { border-right: 1px solid #555; border-left: none}
</style>
<?php }
  $layouts = array(
              '2cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/2cols-sidebar.png',
              '3cols' =>  THEME_FOLDER.'assets/images/blog/3cols.png',
              '3cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/3cols-sidebar.png',
              '4cols' =>  THEME_FOLDER.'assets/images/blog/4cols.png',
              'classic-right' =>  THEME_FOLDER.'assets/images/blog/classic-right.png',
              'classic-left' =>  THEME_FOLDER.'assets/images/blog/classic-left.png'
          );

  $portfolio = array(
              '2cols' =>  '2 Cols',
              '3cols' =>  '3 Cols',
              '4cols' =>  '4 Cols'
          );

?>

<div class="tabbable nav-tabs-custom tabs-<?=is_rtl() == TRUE ? 'right' : 'left'?>" role="tabpanel">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#feature_options">Feature</a></li>
    <li><a data-toggle="tab" href="#network_partners">Network Partners</a></li>
    <li><a data-toggle="tab" href="#lets_help_you">Let's Help You</a></li>
    <li><a data-toggle="tab" href="#form_options">Form Options</a></li>
    <li><a data-toggle="tab" href="#popup_options">Popup Options</a></li>
    <li><a data-toggle="tab" href="#cookie_policy">Cookie Policy</a></li>
    <li><a data-toggle="tab" href="#terms_privacy">Terms and Privacy</a></li>
  </ul>

  <div class="tab-content full-content">
    <div id="feature_options" class="tab-pane fade in active">
      <h3>Feature</h3>
   
      <div class="col-xs-12">
        <?=admin_input_text('Title', 'marm_feature_title', get_option('marm_feature_title'), 'Enter a title.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Title Turkish', 'marm_feature_title_turkish', get_option('marm_feature_title_turkish'), 'Bir başlık girin')?>
      </div>

      <div class="col-xs-12">
        <?=admin_upload('Image background', 'feature_image_background', get_option('feature_image_background'), 'Select banner image / Banner resmi seç.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'feature_content', get_option('feature_content'), 'Enter content.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'feature_content_turkish', get_option('feature_content_turkish'), 'İçeriği gir.')?>
      </div>

    </div>

    <div id="network_partners" class="tab-pane fade">
      <h3>Network Partners</h3>
   
      <div class="col-xs-12">
        <?=admin_input_text('Title', 'marm_network_title', get_option('marm_network_title'), 'Enter a title.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Title Turkish', 'marm_network_title_turkish', get_option('marm_network_title_turkish'), 'Bir başlık girin')?>
      </div>

      <div class="col-xs-12">
        <?=admin_upload('Image background', 'network_image_background', get_option('network_image_background'), 'Select banner image / Banner resmi seç.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'network_content', get_option('network_content'), 'Enter content.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'network_content_turkish', get_option('network_content_turkish'), 'İçeriği gir.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Insert Logo', 'network_logo', get_option('network_logo'), 'Logo ekle.')?>
      </div>
    </div>

    <div id="lets_help_you" class="tab-pane fade">
      <h3>Let's Help You</h3>
   
      <div class="col-xs-12">
        <?=admin_input_text('Title', 'marm_help_title', get_option('marm_help_title'), 'Enter a title.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Title Turkish', 'marm_help_title_turkish', get_option('marm_help_title_turkish'), 'Bir başlık girin')?>
      </div>

      <div class="col-xs-12">
        <?=admin_upload('Image background', 'help_image_background', get_option('help_image_background'), 'Select banner image / Banner resmi seç.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'help_content', get_option('help_content'), 'Enter content.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'help_content_turkish', get_option('help_content_turkish'), 'İçeriği gir.')?>
      </div>

    </div>

    <div id="terms_privacy" class="tab-pane fade">
      <h3>Terms and Privacy Options</h3>
   
      <div class="col-xs-12">
        <?=admin_input_text('Title', 'marm_terms_privacy', get_option('marm_terms_privacy'), 'Enter a title.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Title Turkish', 'marm_terms_privacy_turkish', get_option('marm_terms_privacy_turkish'), 'Bir başlık girin')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Image Title', 'imgtitle_terms_privacy', get_option('imgtitle_terms_privacy'), 'Enter image title.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_input_text('Image Title Turkish', 'imgtitle_terms_privacy_turkish', get_option('imgtitle_terms_privacy_turkish'), 'Resim başlığını girin')?>
      </div>

       <div class="col-xs-12">
          <?=admin_upload('Image background', 'terms_privacy_image_background', get_option('terms_privacy_image_background'), 'Select banner image / Banner resmi seç.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'termsprivacy_content', get_option('termsprivacy_content'), 'Enter content.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'termsprivacy_content_turkish', get_option('termsprivacy_content_turkish'), 'İçeriği gir.')?>
      </div>

    </div>

    <div id="cookie_policy" class="tab-pane fade">
      <h3>Cookie Policy Options</h3>

      <div class="col-xs-12">
        <?=admin_textarea('Content', 'cookie_policy_content', get_option('cookie_policy_content'), 'Enter content.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Content Turkish', 'cookie_policy_content_turkish', get_option('cookie_policy_content_turkish'), 'İçeriği gir.')?>
      </div> 

    </div>

    <div id="form_options" class="tab-pane fade">
      <h3>Form Options</h3>

      <!-- Form section starts -->
      <div class="col-xs-12">
        <?=admin_input_text('Form Heading', 'formheading', get_option('formheading'), 'Set form heading.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Heading', 'formheading_turkish', get_option('formheading_turkish'), 'Form başlığı belirle.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 1', 'formfield_1', get_option('formfield_1'), 'Set form field.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 1', 'formfield_1_turkish', get_option('formfield_1_turkish'), 'Form alanı ayarla.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 2', 'formfield_2', get_option('formfield_2'), 'Set form heading.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 2', 'formfield_2_turkish', get_option('formfield_2_turkish'), 'Form alanı ayarla.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 3', 'formfield_3', get_option('formfield_3'), 'Set form field.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 3', 'formfield_3_turkish', get_option('formfield_3_turkish'), 'Form alanı ayarla.')?>
      </div>

      
      <div class="col-xs-12">
        <?=admin_input_text('Form Field 4', 'formfield_4', get_option('formfield_4'), 'Set form field.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 4', 'formfield_4_turkish', get_option('formfield_4_turkish'), 'Form alanı ayarla.')?>
      </div>

      
      <div class="col-xs-12">
        <?=admin_input_text('Form Field 5', 'formfield_5', get_option('formfield_5'), 'Set form field.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 5', 'formfield_5_turkish', get_option('formfield_5_turkish'), 'Form alanı ayarla.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 6', 'formfield_6', get_option('formfield_6'), 'Set form field.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 6', 'formfield_6_turkish', get_option('formfield_6_turkish'), 'Form alanı ayarla.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 7', 'formfield_7', get_option('formfield_7'), 'Set form field.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Form Field 7', 'formfield_7_turkish', get_option('formfield_7_turkish'), 'Form alanı ayarla.')?>
      </div>

      <div class="col-xs-12">
            <?=admin_input_text('Submit Button Title', 'submitbutton_title', get_option('submitbutton_title'), 'Enter a title.')?>
      </div>
      <div class="col-xs-12">
              <?=admin_input_text('Submit Button Title', 'submitbutton_title_turkish', get_option('submitbutton_title_turkish'), 'Enter a title.')?>
      </div>
      <!-- Form section ends -->

    </div>

    <div id="popup_options" class="tab-pane fade">
      <h3>Popup Options</h3>
   
      <div class="col-xs-12">
        <?=admin_upload('Image background', 'popup_image_background', get_option('popup_image_background'), 'Select banner image / Banner resmi seç.')?>
      </div>

    </div>

  </div>
</div>