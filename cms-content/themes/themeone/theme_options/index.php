<?php if(is_rtl() == TRUE) {?>
<style type="text/css">
.question-icon i { border-right: 1px solid #555; border-left: none}
</style>
<?php }
  $layouts = array(
              '2cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/2cols-sidebar.png',
              '3cols' =>  THEME_FOLDER.'assets/images/blog/3cols.png',
              '3cols-sidebar' =>  THEME_FOLDER.'assets/images/blog/3cols-sidebar.png',
              '4cols' =>  THEME_FOLDER.'assets/images/blog/4cols.png',
              'classic-right' =>  THEME_FOLDER.'assets/images/blog/classic-right.png',
              'classic-left' =>  THEME_FOLDER.'assets/images/blog/classic-left.png'
          );

  $portfolio = array(
              '2cols' =>  '2 Cols',
              '3cols' =>  '3 Cols',
              '4cols' =>  '4 Cols'
          );

?>

<div class="tabbable nav-tabs-custom tabs-<?=is_rtl() == TRUE ? 'right' : 'left'?>" role="tabpanel">
  <ul class="nav nav-tabs">
 <!--   <li class="active"><a data-toggle="tab" href="#general">General options</a></li>-->
    <li class="active"><a data-toggle="tab" href="#header">Homepage Options</a></li>
    <li ><a data-toggle="tab" href="#wearemarm">We Are Marm Options</a></li>
    <li ><a data-toggle="tab" href="#turkeylanding">Turkey Landing Options</a></li>
    <li ><a data-toggle="tab" href="#indialanding">India Landing Options</a></li>
    <li ><a data-toggle="tab" href="#othertourslanding">Other Tours Landing Options</a></li>
    <li ><a data-toggle="tab" href="#marmxclusive">Xclusive Options</a></li>
    <li ><a data-toggle="tab" href="#marmcorporate">Corporate Options</a></li>
    <li ><a data-toggle="tab" href="#marmmice">Mice Options</a></li>
    <li ><a data-toggle="tab" href="#marmwellness">Wellness Options</a></li>
    <li ><a data-toggle="tab" href="#marmgolf">Golf Options</a></li>
    <li ><a data-toggle="tab" href="#marmweddings">Weddings Options</a></li>
    <li ><a data-toggle="tab" href="#contact">Contact Options</a></li>
    <li><a data-toggle="tab" href="#blog">Blog Options</a></li>
    <li><a data-toggle="tab" href="#footer">Footer Options</a></li>
  </ul>

  <div class="tab-content full-content">
<?php /*?>    <div id="general" class="tab-pane fade in active">
      <h3>General options</h3>

      <div class="col-xs-12">
        <?=admin_input_spinner('Posts per page', 'posts_per_page', get_option('posts_per_page'), 'Set number of posts per page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_spinner('Search posts per page', 'search_per_page', get_option('search_per_page'), 'Set number of posts on search page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_spinner('Category posts per page', 'category_per_page', get_option('category_per_page'), 'Set number of posts on category page.')?>
      </div>

    </div><?php */?>

    <div id="header" class="tab-pane fade in active">
    <h3>Homepage Options</h3>
    <?php /*?>      <div class="col-xs-12">
        <?=admin_select('Home page Header style', 'themeone_header_layout', array('Flex slider' => 'slider', 'Text rotate' => 'textrotate', 'Static image' => 'static_image', 'Gradient overlay' => 'gradient'), get_option('themeone_header_layout'), 'Select a home page header style.')?>
      </div>      

      <div class="col-xs-12">
        <?=admin_radio ('Home page Fullscreen header', 'themeone_header_fullscreen', array(1 => 'Fullscreen', 0 => 'Classic'), get_option('themeone_header_fullscreen'), 'Select a home page header layout Fullscreen/Classic.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_select('Home slider', 'home_slider', sliders_list(), get_option('home_slider'), 'Select a slider for home page.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_upload('Textrotate background', 'themeone_textrotate_image', get_option('themeone_textrotate_image'), 'Select background image for Textrotate header.')?>
      </div><?php */?>

      <div class="col-xs-12">
        <?=admin_textarea('Experience  title', 'themeone_textrotate_title', get_option('themeone_textrotate_title'), 'Enter a title for Textrotate header.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Experience  title Turkish', 'themeone_textrotate_title_turkish', get_option('themeone_textrotate_title_turkish'), 'Enter a title for Textrotate header.')?>
      </div>
      
      <div class="col-xs-12">
        <?=admin_textarea('Experience  texts', 'themeone_textrotate', get_option('themeone_textrotate'), 'Enter texts for textslider (separate them by comma ",").')?>
      </div>
      
        <div class="col-xs-12">
        <?=admin_textarea('Experience  texts Turkish', 'themeone_textrotate_turkish', get_option('themeone_textrotate_turkish'), 'Enter texts for textslider (separate them by comma ",").')?>
      </div>
      
<?php /*?>      <div class="col-xs-12">
        <?=admin_upload('Static image background 1', 'themeone_header_image', get_option('themeone_header_image'), 'Select background image for static image header.')?>
      </div>
      <div class="col-xs-12">
        <?=admin_upload('Static image background 2', 'themeone_header_image2', get_option('themeone_header_image2'), 'Select background image for static image header.')?>
      </div><?php */?>
   <div class="col-xs-12">
      <?=admin_input_text('Other Tours Title', 'title_others_tours', get_option('title_others_tours'), 'Enter a title for others tour.')?>
    </div>
    
    <div class="col-xs-12">
      <?=admin_input_text('Other Tours Title Turkish', 'title_others_tours_turkish', get_option('title_others_tours_turkish'), 'Enter a title for others tour.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Other Tours Sidebar Title', 'title_others_tours_sidebar', get_option('title_others_tours_sidebar'), 'Enter a title for others tour.')?>
    </div>
    
    <div class="col-xs-12">
      <?=admin_input_text('Other Tours Sidebar Title Turkish', 'title_others_tours_sidebar_turkish', get_option('title_others_tours_sidebar_turkish'), 'Enter a title for others tour.')?>
    </div>
    
</div>

    <div id="blog" class="tab-pane fade">
      <h3>Blog options</h3>

      <div class="col-xs-12">
        <?=admin_radio_image ('Blog layout', 'themeone_blog_layout', $layouts, get_option('themeone_blog_layout'), 'Select a layout for blog page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio_image ('Category layout', 'themeone_category_layout', $layouts, get_option('themeone_category_layout'), 'Select a layout for category page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio_image ('Search page layout', 'themeone_search_layout', $layouts, get_option('themeone_search_layout'), 'Select a layout for search page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio ('portfolio page layout', 'themeone_portfolio_layout', $portfolio, get_option('themeone_portfolio_layout'), 'Select a layout for portfolio page.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio ('portfolio Boxed', 'themeone_portfolio_boxed', NULL, get_option('themeone_portfolio_boxed'), 'Select portfolio layout Boxed/Full width.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_radio ('portfolio Gutter', 'themeone_portfolio_gutter', NULL, get_option('themeone_portfolio_gutter'), 'Select portfolio gutter layout.')?>
      </div>            

    </div>

    <div id="marmxclusive" class="tab-pane fade">
      <h3>Xclusive Options</h3>
      <div class="col-xs-12">
       
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Title', 'marmxclusive_title', get_option('marmxclusive_title'), 'Set a title.')?>
      </div>
      <div class="col-xs-12">
        <?=admin_input_text('Title Turkish', 'marmxclusive_title_turkish', get_option('marmxclusive_title_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
          <?=admin_upload('Image background', 'marmxclusive_image_background', get_option('marmxclusive_image_background'), 'Select background image for static image header.')?>
      </div>

      <div class="col-xs-12">
          <?=admin_textarea('Enter Content', 'marmxclusive_content', get_option('marmxclusive_content'), 'Enter the content.')?>
      </div>

      <div class="col-xs-12">
          <?=admin_textarea('Enter Content Turkish', 'marmxclusive_content_turkish', get_option('marmxclusive_content_turkish'), 'Enter the content.')?>
      </div>

      <!-- Our services starts -->
      <div class="col-xs-12">
        <?=admin_input_text('Heading', 'marmxclusive_heading', get_option('marmxclusive_heading'), 'Set service heading.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Heading Turkish', 'marmxclusive_heading_turkish', get_option('marmxclusive_heading_turkish'), 'Servis başlığını ayarla.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 1', 'marmxclusive_services1', get_option('marmxclusive_services1'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 1', 'marmxclusive_services1_turkish', get_option('marmxclusive_services1_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 2', 'marmxclusive_services2', get_option('marmxclusive_services2'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 2', 'marmxclusive_services2_turkish', get_option('marmxclusive_services2_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 3', 'marmxclusive_services3', get_option('marmxclusive_services3'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 3', 'marmxclusive_services3_turkish', get_option('marmxclusive_services3_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 4', 'marmxclusive_services4', get_option('marmxclusive_services4'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 4', 'marmxclusive_services4_turkish', get_option('marmxclusive_services4_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 5', 'marmxclusive_services5', get_option('marmxclusive_services5'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 5', 'marmxclusive_services5_turkish', get_option('marmxclusive_services5_turkish'), 'Set a title.')?>
      </div>
      <!-- Our services ends -->
    </div>

    <div id="marmcorporate" class="tab-pane fade">
      <h3>Corporate Options</h3>

     <div class="col-xs-12">
        <?=admin_input_text('Title', 'marmcorporate_title', get_option('marmcorporate_title'), 'Set a title.')?>
      </div>
      <div class="col-xs-12">
        <?=admin_input_text('Title Turkish', 'marmcorporate_title_turkish', get_option('marmcorporate_title_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
          <?=admin_upload('Image background', 'marmcorporate_image_background', get_option('marmcorporate_image_background'), 'Select background image for static image header.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'marmcorporate_content', get_option('marmcorporate_content'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_textarea('Enter Content Turkish', 'marmcorporate_content_turkish', get_option('marmcorporate_content_turkish'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_input_text('Heading', 'marmcorporate_heading', get_option('marmcorporate_heading'), 'Set service heading.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Heading Turkish', 'marmcorporate_heading_turkish', get_option('marmcorporate_heading_turkish'), 'Servis başlığını ayarla.')?>
      </div>

     <div class="col-xs-12">
        <?=admin_input_text('Our services 1', 'marmcorporate_services1', get_option('marmcorporate_services1'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services Turkish 1', 'marmcorporate_services1_turkish', get_option('marmcorporate_services1_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services 2', 'marmcorporate_services2', get_option('marmcorporate_services2'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 2', 'marmcorporate_services2_turkish', get_option('marmcorporate_services2_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services 3', 'marmcorporate_services3', get_option('marmcorporate_services3'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services Turkish 3', 'marmcorporate_services3_turkish', get_option('marmcorporate_services3_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services 4', 'marmcorporate_services4', get_option('marmcorporate_services4'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services Turkish 4', 'marmcorporate_services4_turkish', get_option('marmcorporate_services4_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services 5', 'marmcorporate_services5', get_option('marmcorporate_services5'), 'Set a title.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Our Services Turkish 5', 'marmcorporate_services5_turkish', get_option('marmcorporate_services5_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services 6', 'marmcorporate_services6', get_option('marmcorporate_services6'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our Services Turkish 6', 'marmcorporate_services6_turkish', get_option('marmcorporate_services6_turkish'), 'Set a title.')?>
      </div>
   </div>
    
  


 <div id="marmmice" class="tab-pane fade">
      <h3>Mice Options</h3>

    <div class="col-xs-12">
      <?=admin_input_text('Title', 'marmmice_title', get_option('marmmice_title'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Title Turkish', 'marmmice_title_turkish', get_option('marmmice_title_turkish'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_upload('Image background', 'marmmice_image_background', get_option('marmmice_image_background'), 'Select background image for static image header.')?>
    </div>

     <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'marmmice_content', get_option('marmmice_content'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_textarea('Enter Content Turkish', 'marmmice_content_turkish', get_option('marmmice_content_turkish'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_input_text('Heading', 'marmmice_heading', get_option('marmmice_heading'), 'Set service heading.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Heading Turkish', 'marmmice_heading_turkish', get_option('marmmice_heading_turkish'), 'Servis başlığını ayarla.')?>
      </div>

     <div class="col-xs-12">
        <?=admin_input_text('Our services 1', 'marmmice_services1', get_option('marmmice_services1'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 1', 'marmmice_services1_turkish', get_option('marmmice_services1_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 2', 'marmmice_services2', get_option('marmmice_services2'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 2', 'marmmice_services2_turkish', get_option('marmmice_services2_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 3', 'marmmice_services3', get_option('marmmice_services3'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 3', 'marmmice_services3_turkish', get_option('marmmice_services3_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 4', 'marmmice_services4', get_option('marmmice_services4'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 4', 'marmmice_services4_turkish', get_option('marmmice_services4_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 5', 'marmmice_services5', get_option('marmmice_services5'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 5', 'marmmice_services5_turkish', get_option('marmmice_services5_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 6', 'marmmice_services6', get_option('marmmice_services6'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 6', 'marmmice_services6_turkish', get_option('marmmice_services6_turkish'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 7', 'marmmice_services7', get_option('marmmice_services7'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 7', 'marmmice_services7_turkish', get_option('marmmice_services7_turkish'), 'Set a title.')?>
      </div>
  </div>

<div id="marmwellness" class="tab-pane fade">
      <h3>Wellness Options</h3>

  <div class="col-xs-12">
    <?=admin_input_text('Title', 'marmwellness_title', get_option('marmwellness_title'), 'Set a title.')?>
  </div>

  <div class="col-xs-12">
    <?=admin_input_text('Title Turkish', 'marmwellness_title_turkish', get_option('marmwellness_title_turkish'), 'Bir başlık girin.')?>
  </div>

  <div class="col-xs-12">
    <?=admin_input_text('Subtitle', 'marmwellness_subtitle', get_option('marmwellness_subtitle'), 'Set a Subtitle.')?>
  </div>

  <div class="col-xs-12">
    <?=admin_input_text('Subtitle Turkish', 'marmwellness_subtitle_turkish', get_option('marmwellness_subtitle_turkish'), 'Altyazı ayarlayın..')?>
  </div>

  <div class="col-xs-12">
    <?=admin_upload('Image background', 'marmwellness_image_background', get_option('marmwellness_image_background'), 'Select banner image / Banner resmi seç.')?>
  </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content', 'marmwellness_content', get_option('marmwellness_content'), 'Enter the content.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content Turkish', 'marmwellness_content_turkish', get_option('marmwellness_content_turkish'), 'İçeriği gir.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Heading', 'marmwellness_heading', get_option('marmwellness_heading'), 'Set service heading.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading Turkish', 'marmwellness_heading_turkish', get_option('marmwellness_heading_turkish'), 'Servis başlığını ayarla.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Our services 1', 'marmwellness_services1', get_option('marmwellness_services1'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Our services Turkish 1', 'marmwellness_services1_turkish', get_option('marmwellness_services1_turkish'), 'Bir başlık girin.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Our services 2', 'marwellness_services2', get_option('marwellness_services2'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 2', 'marmwellness_services2_turkish', get_option('marmwellness_services2_turkish'), 'Bir başlık girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 3', 'marmwellness_services3', get_option('marmwellness_services3'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 3', 'marmwellness_services3_turkish', get_option('marmwellness_services3_turkish'), 'Bir başlık girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 4', 'marmwellness_services4', get_option('marmwellness_services4'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 4', 'marmwellness_services4_turkish', get_option('marmwellness_services4_turkish'), 'Bir başlık girin.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Our services 5', 'marmwellness_services5', get_option('marmwellness_services5'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 5', 'marmwellness_services5_turkish', get_option('marmwellness_services5_turkish'), 'Bir başlık girin.')?>
      </div>
</div>

<div id="marmgolf" class="tab-pane fade">
      <h3>Golf Options</h3>

  <div class="col-xs-12">
    <?=admin_input_text('Title', 'marmgolf_title', get_option('marmgolf_title'), 'Set a title.')?>
  </div>

  <div class="col-xs-12">
    <?=admin_input_text('Title Turkish', 'marmgolf_title_turkish', get_option('marmgolf_title_turkish'), 'Bir başlık girin.')?>
  </div>

   <div class="col-xs-12">
    <?=admin_input_text('Subtitle', 'marmgolf_subtitle', get_option('marmgolf_subtitle'), 'Set a Subtitle.')?>
  </div>

  <div class="col-xs-12">
    <?=admin_input_text('Subtitle Turkish', 'marmgolf_subtitle_turkish', get_option('marmgolf_subtitle_turkish'), 'Altyazı ayarlayın..')?>
  </div>

  <div class="col-xs-12">
    <?=admin_upload('Image background', 'marmgolf_image_background', get_option('marmgolf_image_background'), 'Select banner image / Banner resmi seç.')?>
  </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content', 'marmgolf_content', get_option('marmgolf_content'), 'Enter the content.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content Turkish', 'marmgolf_content_turkish', get_option('marmgolf_content_turkish'), 'İçeriği gir.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Heading', 'marmgolf_heading', get_option('marmgolf_heading'), 'Set service heading.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading Turkish', 'marmgolf_heading_turkish', get_option('marmgolf_heading_turkish'), 'Servis başlığını ayarla.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Our services 1', 'marmgolf_services1', get_option('marmgolf_services1'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Our services Turkish 1', 'marmgolf_services1_turkish', get_option('marmgolf_services1_turkish'), 'Bir başlık girin.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Our services 2', 'marmgolf_services2', get_option('marmgolf_services2'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 2', 'marmgolf_services2_turkish', get_option('marmgolf_services2_turkish'), 'Bir başlık girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 3', 'marmgolf_services3', get_option('marmgolf_services3'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 3', 'marmgolf_services3_turkish', get_option('marmgolf_services3_turkish'), 'Bir başlık girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 4', 'marmgolf_services4', get_option('marmgolf_services4'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 4', 'marmgolf_services4_turkish', get_option('marmgolf_services4_turkish'), 'Bir başlık girin.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Our services 5', 'marmgolf_services5', get_option('marmgolf_services5'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 5', 'marmgolf_services5_turkish', get_option('marmgolf_services5_turkish'), 'Bir başlık girin.')?>
      </div>
</div>

<div id="marmweddings" class="tab-pane fade">
      <h3>Weddings Options</h3>

  <div class="col-xs-12">
    <?=admin_input_text('Title', 'marmweddings_title', get_option('marmweddings_title'), 'Set a title.')?>
  </div>

  <div class="col-xs-12">
    <?=admin_input_text('Title Turkish', 'marmweddings_title_turkish', get_option('marmweddings_title_turkish'), 'Bir başlık girin.')?>
  </div>

   <div class="col-xs-12">
    <?=admin_input_text('Subtitle', 'marmweddings_subtitle', get_option('marmweddings_subtitle'), 'Set a Subtitle.')?>
  </div>

  <div class="col-xs-12">
    <?=admin_input_text('Subtitle Turkish', 'marmweddings_subtitle_turkish', get_option('marmweddings_subtitle_turkish'), 'Altyazı ayarlayın..')?>
  </div>

  <div class="col-xs-12">
    <?=admin_upload('Image background', 'marmweddings_image_background', get_option('marmweddings_image_background'), 'Select banner image / Banner resmi seç.')?>
  </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content', 'marmweddings_content', get_option('marmweddings_content'), 'Enter the content.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content Turkish', 'marmweddings_content_turkish', get_option('marmweddings_content_turkish'), 'İçeriği gir.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Heading', 'marmweddings_heading', get_option('marmweddings_heading'), 'Set service heading.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading Turkish', 'marmweddings_heading_turkish', get_option('marmweddings_heading_turkish'), 'Servis başlığını ayarla.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Our services 1', 'marmweddings_services1', get_option('marmweddings_services1'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Our services Turkish 1', 'marmweddings_services1_turkish', get_option('marmweddings_services1_turkish'), 'Bir başlık girin.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_input_text('Our services 2', 'marmweddings_services2', get_option('marmweddings_services2'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 2', 'marmweddings_services2_turkish', get_option('marmweddings_services2_turkish'), 'Bir başlık girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 3', 'marmweddings_services3', get_option('marmweddings_services3'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 3', 'marmweddings_services3_turkish', get_option('marmweddings_services3_turkish'), 'Bir başlık girin.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services 4', 'marmweddings_services4', get_option('marmweddings_services4'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 4', 'marmweddings_services4_turkish', get_option('marmweddings_services4_turkish'), 'Bir başlık girin.')?>
      </div>

       <div class="col-xs-12">
        <?=admin_input_text('Our services 5', 'marmweddings_services5', get_option('marmweddings_services5'), 'Set a title.')?>
      </div>

      <div class="col-xs-12">
        <?=admin_input_text('Our services Turkish 5', 'marmweddings_services5_turkish', get_option('marmweddings_services5_turkish'), 'Bir başlık girin.')?>
      </div>
</div>

<div id="contact" class="tab-pane fade">
      <h3>Contact Options</h3>
   
    <div class="col-xs-12">
      <?=admin_input_text('Title', 'marmcontact_title', get_option('marmcontact_title'), 'Set a title.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Title Turkish', 'marmcontact_title_turkish', get_option('marmcontact_title_turkish'), 'Bir başlık ayarla.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Subtitle', 'marmcontact_subtitle', get_option('marmcontact_subtitle'), 'Set a subtitle.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Subtitle Turkish', 'marmcontact_subtitle_turkish', get_option('marmcontact_subtitle_turkish'), 'Altyazı ayarla.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_upload('Image background', 'marmcontact_image_background', get_option('marmcontact_image_background'), 'Select background image for static image header.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading 1', 'marm_heading1', get_option('marm_heading1'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading Turkish 1', 'marm_heading1_turkish', get_option('marm_heading1_turkish'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_textarea('Enter Content', 'marm_heading1_content', get_option('marm_heading1_content'), 'Enter the content.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading 2', 'marm_heading2', get_option('marm_heading2'), 'Set a Heading.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading Turkish 2', 'marm_heading2_turkish', get_option('marm_heading2_turkish'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'marm_heading2_content', get_option('marm_heading2_content'), 'Enter the content.')?>
      </div>

      <div class="col-xs-12">
      <?=admin_input_text('Heading 3', 'marm_heading3', get_option('marm_heading3'), 'Set a Heading.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_input_text('Heading Turkish 3', 'marm_heading3_turkish', get_option('marm_heading3_turkish'), 'Set a title.')?>
    </div>

    <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'marm_heading3_content', get_option('marm_heading3_content'), 'Enter the content.')?>
    </div>
</div>

<div id="wearemarm" class="tab-pane fade">
      <h3>We Are Marm Options</h3>

    <div class="col-xs-12">
   <?=admin_input_text('Title', 'wearemarm_title', get_option('wearemarm_title'), 'Set a Title.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Title Turkish', 'wearemarm_title_turkish', get_option('wearemarm_title_turkish'), 'Bir Başlık Belirle.')?>
    </div>

   <div class="col-xs-12">
   <?=admin_input_text('Subtitle', 'wearemarm_subtitle', get_option('wearemarm_subtitle'), 'Set a subtitle.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Subtitle Turkish', 'wearemarm_subtitle_turkish', get_option('wearemarm_subtitle_turkish'), 'Altyazı ayarla.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_upload('Image background', 'wearemarm_image_background', get_option('wearemarm_image_background'), 'Select background image for static image header.')?>
    </div>      

    <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'wearemarm_content', get_option('wearemarm_content'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_textarea('Enter Content Turkish', 'wearemarm_content_turkish', get_option('wearemarm_content_turkish'), 'Enter the content.')?>
     </div>
</div>

<div id="turkeylanding" class="tab-pane fade">
      <h3>Turkey Landing Options</h3>

   <div class="col-xs-12">
      <?=admin_input_text('Title', 'turkeylanding_title', get_option('turkeylanding_title'), 'Set a title.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Title Turkish', 'turkeylanding_title_turkish', get_option('turkeylanding_title_turkish'), 'Bir başlık ayarlayın.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_upload('Image background', 'turkeylanding_image_background', get_option('turkeylanding_image_background'), 'Select banner image / Banner resmi seç.')?>
    </div>   

    <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'turkeylanding_content', get_option('turkeylanding_content'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_textarea('Enter Content Turkish', 'turkeylanding_content_turkish', get_option('turkeylanding_content_turkish'), 'İçeriği girin.')?>
     </div>

</div>

<div id="indialanding" class="tab-pane fade">
      <h3>India Landing Options</h3>
   <div class="col-xs-12">
      <?=admin_input_text('Title', 'indialanding_title', get_option('indialanding_title'), 'Set a title.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Title Turkish', 'indialanding_title_turkish', get_option('indialanding_title_turkish'), 'Bir başlık ayarlayın.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_upload('Image background', 'indialanding_image_background', get_option('indialanding_image_background'), 'Select banner image / Banner resmi seç.')?>
    </div>   

    <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'indialanding_content', get_option('indialanding_content'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_textarea('Enter Content Turkish', 'indialanding_content_turkish', get_option('indialanding_content_turkish'), 'İçeriği girin.')?>
     </div>
   
</div>

<div id="othertourslanding" class="tab-pane fade">
      <h3>Other Tours Landing Options</h3>
   <div class="col-xs-12">
      <?=admin_input_text('Title', 'othertourslanding_title', get_option('othertourslanding_title'), 'Set a title.')?>
    </div>
    <div class="col-xs-12">
      <?=admin_input_text('Title Turkish', 'othertourslanding_title_turkish', get_option('othertourslanding_title_turkish'), 'Bir başlık ayarlayın.')?>
    </div>

    <div class="col-xs-12">
      <?=admin_upload('Image background', 'othertourslanding_image_background', get_option('othertourslanding_image_background'), 'Select banner image / Banner resmi seç.')?>
    </div>   

    <div class="col-xs-12">
        <?=admin_textarea('Enter Content', 'othertourslanding_content', get_option('othertourslanding_content'), 'Enter the content.')?>
     </div>

     <div class="col-xs-12">
        <?=admin_textarea('Enter Content Turkish', 'othertourslanding_content_turkish', get_option('othertourslanding_content_turkish'), 'İçeriği girin.')?>
     </div>
   
</div>

<div id="footer" class="tab-pane fade">
      <h3>Footer Options</h3>
 
   <div class="col-xs-12">
      <?=admin_textarea('Footer Content', 'footer_main_content', get_option('footer_main_content'), 'Set a content fot footer widget.')?>
      </div>
      
    <div class="col-xs-12">
      <?=admin_textarea('Footer Content Turkish', 'footer_main_content_turkish', get_option('footer_main_content_turkish'), 'Set a content fot footer widget.')?>
      </div>
       <div class="col-xs-12">
      <?=admin_textarea('Footer Address', 'footer_address_content', get_option('footer_address_content'), 'Set a content fot footer address.')?>
      </div>
      
      <div class="col-xs-12">
      <?=admin_input_text('Footer Phone', 'footer_phone', get_option('footer_phone'), 'Set a footer phone.')?>
      </div>

      <div class="col-xs-12">
      <?=admin_input_text('Footer Email', 'footer_email', get_option('footer_email'), 'Set a footer email.')?>
      </div>
      
    </div>

  </div>
</div>